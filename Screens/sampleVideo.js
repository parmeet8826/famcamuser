import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  Animated,
  AsyncStorage,
  NetInfo
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
var VideoPlayer = require('react-native-native-video-player');

const urltype = 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/taifma88aa9+h89.mp4';

export default class sampleVideo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      videos : [
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/+o0biha0fhht+n+.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/0ef4n++s5ba4uca.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/2fJJ7Jabs50s33a.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a2g6af8fe7va217.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a9a58a8428dc1a2.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      }
      ]
    }
  }

	static navigatorStyle = {
		navBarHidden : true
	}

  hello = (url) => {
    VideoPlayer.showVideoPlayer(url);
  }

	render() {
    console.log(urltype);
    let {videos} = this.state;
		return (
			 <View style = {{flex:1, marginHorizontal: 24}}>
          <ScrollView style = {{flex:1}} showsVerticalScrollIndicator = {false}>

            {
              videos.map((value,index) => {
                return <View style = {{flex:1}} key = {index}>

                  <View style = {{width: 312, height: 232, marginTop:24, flexDirection:'row', borderBottomWidth:0.3, borderBottomColor : '#ACACAC'}}>
                    <View style = {{width:126, height: 208}}>
                        <TouchableOpacity activeOpacity = {0.8}>
                        <Image source = {{uri : 'https://wallpaperscraft.com/image/justin_bieber_face_gesture_hand_black_and_white_61060_1920x1080.jpg'}} style = {{width:126, height:208}}/>
                        <Image source  = {require('./../Images/GroupCopy3x.png')} style = {{width:24, height: 24, position: 'absolute', top: 176, left:5}}/>
                        <Text style = {{fontSize:12, lineHeight:16, fontWeight:'bold', color : 'white', position:'absolute', top:10, left:90}}>00:30</Text>
                      </TouchableOpacity>
                    </View>
                    <View style = {{marginLeft: 10, width: 130}}>
                      <Text style = {{fontSize: 20, lineHeight: 24, color : '#1F1D1D', fontWeight:'bold', height:40}}>Drake</Text>
                      <Text style = {{fontWeight:'bold', fontSize:14, lineHeight: 16, color : '#474D57', opacity: 0.5}}>For</Text>
                      <Text style = {{fontSize:14, lineHeight: 24, color: 'black', height: 50}}>Troy McCarthy</Text>
                      <Text style = {{fontSize:14, lineHeight: 16, color : '#474D57', fontWeight:'bold', opacity: 0.5, height:20}}>MESSAGE</Text>
                      <Text style = {{fontSize:14, lineHeight: 20, color : 'black', opacity: 0.8}}>May you have all the joy your heart can hold. May your day be as...</Text>
                      <Text style = {{fontSize:12, lineHeight: 16, color : '#7F7F7F', marginTop: 20}}>12 mins ago</Text>
                    </View>
                  </View>

                </View>
              })
            }

          </ScrollView>
			 </View>
		)
	}
}
