import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';

class registerPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      show_password: true,
      isPasswordValid : false,
      visible: false
    }
  }

  static navigatorStyle = {
    navBarHidden : true,
    statusBarHidden : true
  }

  back = () => {
    this.props.navigator.pop();
  }

  showPassword = () => {
    let {show_password} = this.state;
    if(show_password) {
      this.setState({show_password:false})
    }
    else {
      this.setState({show_password:true})
    }
  }

  checkPassword = (pwd) => {
    var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z_@#*%!&^/()=]{6,}$/;
    if(pwd.length < 6 && pwd.length > 16) {
        this.setState({isPasswordValid: true})
    }
    else if(!pwd.match(regex)) {
      this.setState({isPasswordValid: true})
    }
    else {
      this.setState({isPasswordValid: false})
      this.setState({password: pwd});
    }
  }

  showErrorPassword = () => {
    let {isPasswordValid, password} = this.state;
    if(isPasswordValid == true) {
      return <Text style = {{color: 'red', marginTop:10}}>Password Length should be 6 to 16. Must Contain a number, special character, digits.</Text>
    }
  }

  setPassword = () => {
    let {isPasswordValid, password} = this.state;
    this.setState({visible: true});
    if(isPasswordValid === true) {
      Alert.alert(
          '',
          'Password is not correct',
          [
              {
                      text: 'Okay',
                      onPress: () => {
                      this.setState({visible: false});
              } }
          ],
          { cancelable: false }
      );
    }
    else {
      console.log(this.props.user.userId);
      let values = {'userId': this.props.user.userId, 'password': password};
      return axios.post(`${Appurl.apiUrl}setUserPassword`, values)
      .then((response) => {
        return this.getLoggedIn(response);
      }).catch((error) => {
        console.log(error.response);
      })
    }
  }

  getLoggedIn = async(response) => {
    try {
    this.setState({visible: false});
    await AsyncStorage.setItem('user', JSON.stringify(this.props.user.emailId));
    Navigation.startTabBasedApp({
      tabs: [
    {
      label: 'Home',
      screen: 'famcamHome',
      icon: require('./../Images/ic_home.png'),
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
      title: 'Home',
    },
    {
      label: 'Orders',
      screen: 'orders',
      icon: require('./../Images/clipboards.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Orders',
    },
    {
      label: 'Profile',
      screen: 'profile',
      icon: require('./../Images/ic_profile_grey.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Profile',
    },
  ],
  tabsStyle: {
    tabBarButtonColor: '#C54C72',
    tabBarSelectedButtonColor: '#C54C72',
    tabBarBackgroundColor: 'white',
    initialTabIndex: 0,
  },
    })
  }
  catch(error) {}
  }

  render () {
    let {password, show_password} = this.state;
    return (
      <View style={{flex:1, marginHorizontal: 24}}>

        <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>
        <View style={{flex:0.08, justifyContent: 'flex-start'}}>
          <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold'}}>{strings('registerPassword.register')}</Text>
        </View>
        <View style={{flex:0.09}}>
          <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}>{strings('registerPassword.heading', {email: this.props.user.emailId})}</Text>
        </View>
        <View style = {{flex:0.15, flexDirection: 'row-reverse'}}>
          <TouchableOpacity style={{justifyContent: 'center', marginBottom: 17, paddingRight:-70}} onPress={() => {this.showPassword()}}>
            <Image source={require('./../Images/ic_eye.png')} style={{height: 12, width: 20}}/>
          </TouchableOpacity>
          <MKTextField
            placeholder = {strings('registerPassword.placeholder')}
            ref="password"
            placeholderTextColor='#474d57'
            floatingLabelEnabled
            password={show_password}
            keyboardType = "default"
            returnKeyType = "next"
            textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
            style = {{marginTop:10, width: Dimensions.get('window').width*0.8}}
            underlineSize={1}
            highlightColor='#474D57'
            tintColor='red'
            autoCorrect={false}
            autoCapitalize= 'none'
            onChangeText = {(password) => {this.checkPassword(password)}}
          />
        </View>
        {this.showErrorPassword()}
        <View style = {{flex:0.1,alignItems : 'flex-end'}}>
          <TouchableOpacity activeOpacity={0.5} onPress={() => {this.setPassword()}}>
            <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

}


function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(registerPassword);
