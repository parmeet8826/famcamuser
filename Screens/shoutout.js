import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
  ScrollView
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import LinearGradient from 'react-native-linear-gradient';
import Validation from './../src/utils/Validation.js';

class shoutout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name : '',
      email : '',
      instructions : '',
      promo : 0,
      promoApplied : false
    }
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  componentWillMount() {
    console.log(this.props.user.talentName);
  }

  static navigatorStyle = {
    navBarHidden : true,
    tabBarHidden : true
  }

  validationRules= () => {
      return [
          {
              field: this.state.name,
              name: 'Name',
              rules: 'required|alpha|min:2|max:30'
          },
          {
              field: this.state.email,
              name: 'Email Id',
              rules: 'required|email|max:100|no_space'
          },
          {
            field: this.state.instructions,
            name : 'Instructions',
            rules : 'required|alpha|max:100'
          },
          {
            field : this.state.promo,
            name : 'Promo Code',
            rules : 'no_space'
          }
      ]
  }

  back = () => {
    this.props.navigator.pop()
  }


  sendRequest = () => {
    let {actions} = this.props;
    let {visible} = this.state;
    let {name, email, instructions, promo} = this.state;

    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
    return Alert.alert(
    '',
    validaton[0],
    [
      {
        text: 'Okay',
        onPress: ()=> {

        }
      }
    ],
    { cancelable: false }
  );
}

    else {
    this.setState({visible: true});
    let name1 = this.props.user.talentName ? this.props.user.talentName : 'Unknown';
    let values = {'userId' : this.props.user.loginFieldId.id, 'forWhome' : name, 'talentId' : this.props.user.talentId, 'foremail': email, 'message': instructions, 'amountPaid' : 50}
    console.log(values);
    return axios.post(`${Appurl.apiUrl}bookVedioShoutout`, values)
    .then((response) => {
      console.log(response);
      actions.userToken(response.data.clientToken.clientToken);
      return this.abc(response);
    }).catch((error) => {
      console.log(error.response);
      return this.showErrorAlert(error.response);
    })
  }
}

  showErrorAlert = (response) => {
    Alert.alert(
        '',
        response.data.msg,
        [
            {
                    text: 'Okay',
                    onPress: () => {
                    this.setState({visible: false});
            } }
        ],
        { cancelable: false }
    );
  }

  abc = (response) => {
    Alert.alert(
        '',
        'Your Request has been sent',
        [
            {
                    text: 'Okay',
                    onPress: () => {
                    this.setState({visible: false, name : '', email : '', instructions:'', promo:''});
                    this.props.navigator.push({
                      screen : 'paymentPaypal'
                    })
            } }
        ],
        { cancelable: false }
    );
  }


  applyPromo = () => {
    let {promo} = this.state;
    if(promo == '') {
      Alert.alert(
          '',
          'Enter Promo Code To Activate',
          [
              {
                      text: 'Okay',
               }
          ],
          { cancelable: false }
      );
    }

    else {
      Alert.alert(
          '',
          'Your Promo Code Has Been Applied',
          [
              {
                      text: 'Okay',
                      onPress: () => {
                      this.setState({promoApplied: true});
              } }
          ],
          { cancelable: false }
      );
    }
  }

    render() {
      let {name, email, instructions, promo} = this.state;
      return (
        <View style = {{flex:1}}>
        <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />
        <View style = {{flex:0.9}}>
        <ScrollView style = {{flex:1}}>
          <View style={{flex: 0.15, justifyContent: 'center', marginTop: 10,marginHorizontal: 24}}>
            <TouchableOpacity hitSlop = {{top:10, left:10, bottom:10, right:10}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
              <Image source={require('./../Images/ic_cancell.png')} style={{height: 14, width:18}}/>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.14, justifyContent: 'flex-start', marginTop: 10,marginHorizontal: 24}}>
            <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold'}}>{strings('shoutout.text', {name: this.props.user.talentName ? this.props.user.talentName: null})}</Text>
          </View>
          <View style = {{flex:0.13, marginTop:10,marginHorizontal: 24}}>
              <Text>{strings('shoutout.input1')}</Text>
              <TextInput
                placeholder = "Enter Name"
                value = {name}
                autoFocus = {true}
                returnKeyType = "next"
                underlineColorAndroid = "transparent"
                style = {{borderBottomWidth:1, borderBottomColor : '#EBEBEB'}}
                onChangeText = {(name) => {this.setState({name})}}
                onSubmitEditing={(event) => {
                  this.refs.Second.focus();
                }}
              />
          </View>

          <View style = {{flex:0.18, marginTop: 10,marginHorizontal: 24}}>
              <Text>{strings('shoutout.input2')}</Text>
              <TextInput
                placeholder = "Enter Email"
                ref = "Second"
                value = {email}
                underlineColorAndroid = "transparent"
                returnKeyType = "next"
                keyboardType = "email-address"
                style = {{borderBottomWidth:1, borderBottomColor : '#EBEBEB'}}
                onChangeText = {(email) => {this.setState({email})}}
                onSubmitEditing = {(event) => {this.refs.Third.focus();}}
              />
          </View>

          <View style = {{flex:0.18, marginTop: 10,marginHorizontal: 24}}>
              <Text>{strings('shoutout.input3')}</Text>
              <TextInput
                placeholder = "Enter Instructions"
                ref = "Third"
                multiline = {true}
                returnKeyType = "next"
                value = {instructions}
                underlineColorAndroid = "transparent"
                style = {{borderBottomWidth:1, borderBottomColor : '#EBEBEB'}}
                onChangeText = {(instructions) => {this.setState({instructions})}}
                onSubmitEditing = {(event) => {this.refs.Fourth.focus();}}
              />
          </View>

          <View style = {{flex:0.14, flexDirection:'row', borderBottomWidth:1, borderBottomColor : '#EBEBEB', justifyContent: 'center', alignItems:'center', marginTop: 10, marginHorizontal: 24}}>
            <View style = {{flex: 0.8}}>
              <Text>{strings('shoutout.input4')}</Text>
              <TextInput
                placeholder = "Enter Promo Code"
                ref = "Fourth"
                value = {promo}
                returnKeyType = "done"
                underlineColorAndroid = "transparent"
                onChangeText = {(promo) => {this.setState({promo})}}
              />
            </View>
              <TouchableOpacity style = {{flex:0.2,justifyContent:'center'}} onPress = {() => {this.applyPromo()}}>
                <LinearGradient colors={['#8D3F7D', '#D8546E']} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                  <Text style = {{textAlign:'center', padding: 7, color: 'white'}}>Apply</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            </ScrollView>
          </View>

              <TouchableOpacity style = {{flex:0.1, justifyContent:'flex-end'}} onPress = {() => this.sendRequest()}>
                <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{flex:0.9, justifyContent:'center', alignItems:'center'}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                  <View style = {{flex:0.8,justifyContent:'center', alignItems:'flex-start'}}>
                    <Text style = {{color : 'white', fontSize: 16, lineHeight: 24, fontWeight:'bold'}}>Pay SAR {this.state.promoApplied ? (this.props.user.price - promo) : this.props.user.price}</Text>
                  </View>
                </LinearGradient>
              </TouchableOpacity>

        </View>
      )
    }
}

const styles = StyleSheet.create({
  backButton : {
    margin: 20,
    maxHeight:10
  },

  registerHeading : {
    marginLeft: 20,
    marginTop:15
  },

  text : {
    margin:20,
    maxHeight:10
  },

  emailTextField : {
    margin:20,
    backgroundColor : 'red'
  },

  phoneNumberTextField : {
    marginHorizontal: 20,
    backgroundColor : 'green',
    justifyContent : 'flex-start',
    alignItems : 'flex-start',
  },

  passwordTextField : {
    marginHorizontal: 20,
    backgroundColor : 'black',
    justifyContent : 'flex-start',
    alignItems : 'flex-start'
  },

  button : {
    flexDirection : 'row',
    justifyContent : 'flex-end',
    alignItems : 'flex-end',
    marginRight: 20,

  }

})

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(shoutout);
