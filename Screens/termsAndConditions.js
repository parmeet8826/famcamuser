import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class termsAndConditions extends Component {

  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true
  }

  back = () => {
    this.props.navigator.pop();
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  render() {
    return (
      <View style = {{flex:1, marginHorizontal: 24}}>
        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>

        <View style = {{flex:0.1, justifyContent:'center'}}>
          <Text style = {{fontWeight:'bold', fontSize:24, lineHeight:24, color:'#4A4A4A'}}>Terms and Condition</Text>
        </View>

        <View style = {{flex:0.7, marginTop:10}}>
          <Text style = {{fontSize:14, lineHeight: 24, color:'#4A4A4A'}}>Your terms and conditions goes here</Text>
        </View>

      </View>
    )
  }

}


function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(termsAndConditions);
