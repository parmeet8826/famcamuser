import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import Validation from './../src/utils/Validation.js';
import OneSignal from 'react-native-onesignal';

class login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      emailPhone : '',
      visible : false,
      userid : '',
      password : '',
      show_password: true,
    }
  }

  static navigatorStyle = {
    navBarHidden : true,
  }
  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  showPassword = () => {
    let {show_password} = this.state;
    if(show_password) {
      this.setState({show_password:false})
    }
    else {
      this.setState({show_password:true})
    }
  }

  validationRules= () => {
      return [
          {
              field: this.state.password,
              name: 'Password',
              rules: 'required|no_space|min:6|max:15'
          },
      ]
  }

  back = () => {
    let {actions} = this.props;
    actions.toggleButton(false);

    this.props.navigator.pop();
  }

  loginPassword = async() => {
    let {emailPhone, visible, password} = this.state;

    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
    return Alert.alert(
    '',
    validaton[0],
    [
      {
        text: 'Okay',
        onPress: ()=> {

        }
      }
    ],
    { cancelable: false }
  );
}

    else {
    this.setState({visible : true});
    let {actions} = this.props;
    actions.getLoginField(emailPhone);
    OneSignal.sendTag("phone", emailPhone);
    let values = {'authfield' : emailPhone, 'password' : password};
    console.log(values);
    axios.post(`${Appurl.apiUrl}loginUser`, values)
    .then((response) => {
      console.log(response)
      this.setLoginPassword(response)
    }).catch((error) => {
      if(error.response.data.success == 0) {
        Alert.alert(
            '',
            error.response.data.msg,
            [
                {
                        text: 'Okay',
                        onPress: () => {
                        this.setState({visible: false});
                } }
            ],
            { cancelable: false }
          );
        }
      })
    }
  }

  forgetPassword = () => {
    this.props.navigator.push({
      screen : 'forgotPassword'
    })
  }

  setLoginPassword = async(response) => {
    console.log(response);
    let {visible} = this.state;
    let {actions} = this.props;
    console.log(response);
    // let userid = response.data.userId;
    // actions.getLoginUserId(userid);
    try {
    this.setState({visible: false});
    let details = {'image': response.data.Profilepicurl , 'name': response.data.name , 'id': response.data.userId}
    await AsyncStorage.setItem('user', JSON.stringify(details));
    Navigation.startTabBasedApp({
      tabs: [
    {
      label: 'Home',
      screen: 'famcamHome',
      icon: require('./../Images/ic_home.png'),
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
      title: 'Home',
    },
    {
      label: 'Orders',
      screen: 'orders',
      icon: require('./../Images/clipboards.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Orders',
    },
    {
      label: 'Profile',
      screen: 'profile',
      icon: require('./../Images/ic_profile_grey.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Profile',
    },
  ],
  tabsStyle: {
    tabBarButtonColor: '#C54C72',
    tabBarSelectedButtonColor: '#C54C72',
    tabBarBackgroundColor: 'white',
    initialTabIndex: 0,
  },
    })
  }
  catch(error) {}
}

  render() {
      let {emailPhone, password, show_password} = this.state;
      return (
        <View style={{flex:1, marginHorizontal: 24}}>

          <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

          <View style={{flex: 0.1, justifyContent: 'center'}}>
            <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
              <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.09, justifyContent: 'flex-start'}}>
            <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold', color: '#000000'}}>{strings('login.login')}</Text>
          </View>
          <View style={{flex:0.08}}>
            <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}>{strings('login.heading')}</Text>
          </View>
          <View style = {{flex:0.15}}>
              <MKTextField
                placeholder = {strings('login.placeholder')}
                ref="emailPhone"
                placeholderTextColor='#AAAFB9'
                floatingLabelEnabled
                keyboardType = "email-address"
                returnKeyType = "next"
                textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
                style = {{marginTop:10}}
                underlineSize={1}
                highlightColor='#474D57'
                tintColor='#C2567A'
                autoCorrect={false}
                autoCapitalize= 'none'
                onChangeText = {(emailPhone) => this.setState({emailPhone})}
              />
          </View>

          <View style = {{flex:0.12, flexDirection: 'row'}}>
            <MKTextField
              placeholder = {strings('login.placeholder2')}
              ref="password"
              placeholderTextColor='#AAAFB9'
              floatingLabelEnabled
              password={show_password}
              keyboardType = "default"
              returnKeyType = "done"
              textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
              style = {{marginTop:10, flex:0.99}}
              underlineSize={1}
              highlightColor='#474D57'
              tintColor='#C2567A'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText = {(password) => {this.setState({password})}}
            />
          </View>

          <TouchableOpacity style={{flex:0.1, justifyContent: 'center', marginTop: 10}} onPress = {() => {this.forgetPassword()}}>
            <Text style={{fontSize: 14, lineHeight: 16, color: 'black', fontWeight: 'bold'}}> {strings('login.forgotPassword')} </Text>
          </TouchableOpacity>

          <View style = {{flex:0.1,alignItems : 'flex-end'}}>
            <TouchableOpacity activeOpacity={0.5} onPress = {() => {this.loginPassword()}}>
              <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
            </TouchableOpacity>
          </View>
        </View>
      )
    }


}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(login);
