import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Alert,
  TextInput,
  ScrollView,
  Image,
  AsyncStorage
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import LinearGradient from 'react-native-linear-gradient';

var VideoPlayer = require('react-native-native-video-player');

class talentInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data : '',
      image : '',
      name : '',
      profession : '',
      rTalent : [],
      newrTalent: [],
      rVideos : [],
      videos : [
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/+o0biha0fhht+n+.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/0ef4n++s5ba4uca.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/2fJJ7Jabs50s33a.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a2g6af8fe7va217.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a9a58a8428dc1a2.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      }
      ]
    }
  }

  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }


  componentWillMount() {
    this.getTalentInfo();
  }

  // 5a82cdc7b72f2a0004da41ef
  // 5a8287fcf6cda100044e737e
  // Musician

  getTalentInfo = () => {
    let talentInfo1 = {'userId' : this.props.user.loginFieldId.id, 'talentId' : this.props.user.talentId, 'related': this.props.user.related}
    //let talentInfo1 = {'userId' : '5a82cdc7b72f2a0004da41ef', 'talentId' : '5a8287fcf6cda100044e737e', 'related': 'Musician'}
    console.log(talentInfo1);
    return axios.post(`${Appurl.apiUrl}fetchTalentInform`, talentInfo1)
    .then((response) => {
      console.log(response.data);
      return this.displayInfo(response);
    }).catch((error) => {
      console.log(error.response);
    })
  }

  displayInfo = (response) => {
    let {actions} = this.props;
    let {rTalent, name, rVideos} = this.state;
    console.log(response.data.data);
    console.log(response.data.data.talentData.price[0])
    this.setState({image: response.data.data.talentData.profilePicUrl});
    this.setState({name: response.data.data.talentData.name});
    this.setState({profession: response.data.data.talentData.professions[0]});

    actions.setImageTalent(response.data.data.talentData.profilePicUrl);
    actions.getPrice(response.data.data.talentData.price[0]);

    response.data.data.talentVedios.forEach((item,index) => {
      rVideos.push({'id': item._id, 'video': item.vedioUrl, 'duration': item.duration, 'for': item.forWhome, 'image': item.thumbnailUrl});
    });
    this.setState({rVideos});

    response.data.data.relatedTalent.forEach((item, index) => {
      rTalent.push({'id': item._id, 'profession' : item.professions[0], 'pic': item.profilePicUrl, 'name': item.name});
    });
    this.setState({rTalent});
  }

  newTalent = (id) => {
    let {actions} = this.props;
    actions.getTalentId(id);
    this.setState({rTalent: [], rVideos : []});
    let talentInfo1 = {'userId' : this.props.user.loginFieldId.id ? this.props.user.loginFieldId.id : this.props.user.userId, 'talentId' : id, 'related': this.props.user.related}
    console.log(talentInfo1);
    return axios.post(`${Appurl.apiUrl}fetchTalentInform`, talentInfo1)
    .then((response) => {
      console.log(response.data._id);
      return this.showNewTalent(response);
    }).catch((error) => {
      console.log(error.response);
    })
  }

    showNewTalent = (response) => {
      let {rTalent} = this.state;
      let {actions} = this.props;
      this.setState({image: response.data.data.talentData.profilePicUrl});
      this.setState({name: response.data.data.talentData.name});
      console.log(this.props.user.related);
      this.setState({profession: this.props.user.related == response.data.data.talentData.professions[0] ? response.data.data.talentData.professions[0] : response.data.data.talentData.professions[1]});

      actions.setTalentName(response.data.data.talentData.name);
      actions.setImageTalent(response.data.data.talentData.profilePicUrl);
      actions.getPrice(response.data.data.talentData.price[0]);

      response.data.data.talentVedios.forEach((item,index) => {
        rVideos.push({'id': item._id, 'video': item.vedioUrl, 'duration': item.duration, 'for': item.forWhome, 'image': item.thumbnailUrl});
      });
      this.setState({rVideos});

      response.data.data.relatedTalent.forEach((item, index) => {
        rTalent.push({'id': item._id, 'profession' : this.props.user.related == item.professions[0] ? item.professions[0] : item.professions[1], 'pic': item.profilePicUrl, 'name': item.name});
      });
      this.setState({rTalent});
    }

  back = () => {
    this.props.navigator.pop();
  }

  requestPage = () => {
    let {actions} = this.props;
    actions.toggleButton(true);

    this.props.navigator.push({
      screen : 'shoutout'
    })
  }

  onSlide() {
    let {actions} = this.props;
    actions.toggleButton(true);

      this.props.navigator.push({
        screen : 'shoutout'
      })
  }

  // <SlideButton
  //   style = {{justifyContent:'flex-end', flex:1}}
  //   onSlideSuccess={this.onSlide.bind(this)}
  //    slideDirection={SlideDirection.LEFT}
  //    width={500}
  //    height={50}>

  playVideo = (url) => {
    VideoPlayer.showVideoPlayer(url);
  }

  render() {
    let {data, image, name, profession, result, rTalent, newName, newImage, newProfession, videos, rVideos} = this.state;
    return (
      <View style = {{flex:1}}>

        <ScrollView style = {{flex:1}}>

          <View style = {{flex:0.5}}>
            <ImageBackground source = {{uri : image }} style = {{width:400, height: 400}}>
              <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center', margin: 20}} onPress={() => {this.back()}}>
                <Image source={require('./../Images/ic_back_white.png')} style={{height: 14, width:18}}/>
              </TouchableOpacity>
            </ImageBackground>

            <View style = {{flex: 0.2, backgroundColor : 'white', marginTop: -70, borderRadius:5, margin:2}}>
              <Text style = {{fontSize: 20, fontWeight: 'bold', marginTop: 20, marginLeft:20, color : '#1F1D1D'}}>{name}</Text>
              <Text style = {{marginTop:5, marginLeft:20, color: '#4A4A4A', fontSize: 14, lineHeight: 24}}>{profession}</Text>


              <View style = {{flex:0.2}}>
                <Text style = {{fontSize: 16, lineHeight: 24, fontWeight:'bold', color : '#1F1D1D', margin: 20}}>Latest</Text>
                <ScrollView horizontal = {true} showsHorizontalScrollIndicator = {false}>
                    {
                      rVideos ? rVideos.map((value, index) => {
                        return <View key = {index} style = {{flex:1, marginHorizontal:20}}>
                          <View style = {{width: 144, height: 232}}>
                            <TouchableOpacity activeOpacity = {0.8} onPress = {() => {this.playVideo(value.video)}}>
                              <Image source = {{uri : value.image}} style = {{width:144, height: 192, borderRadius: 4}}/>
                              <Image source  = {require('./../Images/GroupCopy3x.png')} style = {{width:24, height: 24, position: 'absolute', top: 160, left:5}}/>
                              <Text style = {{fontSize:12, lineHeight:16, fontWeight:'bold', color : 'white', position:'absolute', top:10, left:100}}>{value.duration}</Text>
                            </TouchableOpacity>
                            <View style = {{flex:0.3}}></View>
                            <View style = {{flexDirection:'row'}}>
                              <Text style = {{fontSize:14, lineHeight: 16, color : '#343434'}}>For  </Text>
                              <Text style = {{fontSize:14, lineHeight: 16, fontWeight:'bold', color : '#343434'}}>{value.for}</Text>
                            </View>
                          </View>
                        </View>
                      }) : null
                    }
                </ScrollView>
              </View>

              <View style = {{flex:0.3}}>
                <Text style = {{fontSize: 16, lineHeight:24, fontWeight:'bold', margin: 20, color : '#1F1D1D'}}>Related Talent</Text>
                <ScrollView horizontal = {true} showsHorizontalScrollIndicator ={false}>
                  {
                    rTalent ? rTalent.map((value, index) => {
                      return <View style = {{flex:1, flexDirection:'row'}} key = {index}>
                        <View style = {{flex:0.7, marginLeft:20}}>
                          <TouchableOpacity activeOpacity = {0.7} onPress = {() => {this.newTalent(value.id)}}>
                            <Image source = {{uri: value.pic}} style = {{width: 100, height: 100, borderRadius:10}}/>
                          </TouchableOpacity>
                          <Text style = {{fontWeight:'bold', fontSize:14}}>{value.name}</Text>
                          <Text>{value.profession}</Text>
                        </View>
                      </View>
                    }) : null
                  }
                </ScrollView>
              </View>


            </View>

          </View>

        </ScrollView>

        <View style = {{flex:0.1, flexDirection:'row', borderWidth: 2, borderTopColor: 'white', borderLeftColor: 'white', marginTop:2}}>
          <View style = {{flex:0.7,justifyContent:'center', alignItems:'flex-start'}}>
            <Text style = {{color : '#BF4D73', fontSize: 16, lineHeight: 24, fontWeight:'bold', marginLeft:30}}>Book For SAR {this.props.user.price} </Text>
          </View>


          <TouchableOpacity disabled = {this.props.user.isDisabled} style = {{flex:0.3, height:135}} onPress = {() => {this.requestPage()}}>
            <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{flex:0.4, justifyContent:'center', alignItems:'center'}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
              <View style = {{justifyContent:'center', alignItems:'center'}}>
                <Text style = {{color: 'white', fontWeight: '500', fontSize: 14, lineHeight: 18, textAlign:'center'}}>Book Now</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(talentInfo);
