import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
  AsyncStorage,
  PermissionsAndroid
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import { RNS3 } from 'react-native-aws3';
import Validation from './../src/utils/Validation.js';

const bca = '';
const option = {
  keyPrefix: "ImagesUser/",
  bucket: "famcamuploads",
  region: "us-east-2",
  accessKey: "AKIAI4LEFCTKJNKI63IQ",
  secretKey: "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",
  successActionStatus: 201
};

var ImagePicker = require('react-native-image-picker');
var options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

class editProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
        name : '',
        phone : '',
        email : '',
        avatarSource : null,
        visible : false,
        photo : ''
    }
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true
  }

  componentWillMount() {
    let id = {'userId': this.props.user.loginFieldId.id};
    return axios.post(`${Appurl.apiUrl}getUserImage`, id)
    .then((response) => {
      return this.getResult(response.data);
    }).catch((error) => {
      console.log(error.response);
    })
  }

  getResult = (data) => {
    let {name, phone, email, photo} = this.state;
    this.setState({name: data.name, phone: data.phoneNumber, email: data.email, photo: data.Profilepicurl});
  }

  validationRules= () => {
      return [
          {
              field: this.state.name,
              name: 'Name',
              rules: 'required|max:30'
          },
          {
              field: this.state.phone,
              name: 'Phone',
              rules: 'required|no_space'
          },
          {
            field: this.state.email,
            name : 'Phone',
            rules: 'required|email|max:100|no_space'
          }

      ]
  }

  back = () => {
    let {actions} = this.props;
    actions.toggleButton(false);

    this.props.navigator.pop();
  }

  image = async() => {

    let {avatarSource} = this.state;
    let {actions} = this.props;

    if(Platform.OS == 'android' && Platform.Version > 22){

        const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            ]
        );

        if (granted['android.permission.WRITE_EXTERNAL_STORAGE'] != 'granted' || granted['android.permission.CAMERA'] != 'granted')
            return Alert.alert('', "Don't have permissions to select image.");
    }
    ImagePicker.showImagePicker(options, (response) => {
      let {avatarSource} = this.state;
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else if(!response.error && !response.didCancel) {
       avatarSource = 'data:image/jpeg;base64,' + response.data;
       this.setState({avatarSource});
      bca = response.uri;
      }
      // actions.getImage(avatarSource);
    });
  }

  saveDetails = () => {
    let {name, avatarSource, phone} = this.state;
    let {actions} = this.props;
    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
    return Alert.alert(
    '',
    validaton[0],
    [
      {
        text: 'Okay',
        onPress: ()=> {

        }
      }
    ],
    { cancelable: false }
  );
}

    else {
    this.setState({visible: true});
    var textorder = '';
      var possible = this.props.user.loginFieldId.id;
      for(var i = 0; i < 10; i++) {
        textorder += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      const file = {
        uri: bca,
        name: textorder+'.jpg',
        type: 'image/jpg'
      }

      RNS3.put(file,option).then(response => {
        if(response.status !== 201)
          throw new Error("Failed to upload image to S3")

        console.log(response.body);
        actions.getImage(response.body.postResponse.location);
        let values = {'userId' : this.props.user.loginFieldId.id, 'name' : name, 'Profilepicurl' : response.body.postResponse.location}
        console.log(values);
        return axios.post(`${Appurl.apiUrl}edituserProfileformdeshboard`, values)
          .then((response) => {
            return this.detailsSaved(response);
          }).catch((error) => {
            Alert.alert(
                        '',
                        error.response.data.msg,
                        [
                            {
                                    text: 'Okay',
                                    onPress: () => {
                                    this.setState({visible: false});
                            } }
                        ],
                        { cancelable: false }
                    );
        })
      }
    )



  }
}

  detailsSaved = (response) => {
    Alert.alert(
        '',
        'Your Details has been updated',
        [
            {
                    text: 'Okay',
                    onPress: () => {
                    this.setState({visible: false});
                    this.props.navigator.pop();
            } }
        ],
        { cancelable: false }
    );
  }

  showImage = () => {
    let {avatarSource, photo} = this.state;
    if(avatarSource) {
      return <Image source = {{uri: avatarSource}} style = {{width: 80, height: 80, borderRadius: 40, opacity: 0.8}}/>
    }
    else {
      return <Image source = {{uri: this.props.user.profilepic ? this.props.user.profilepic : this.props.user.loginFieldId.image}} style = {{width:80, height:80, borderRadius: 40, opacity: 0.8}}/>
    }
  }

  render() {
    let {name, phone, email, avatarSource} = this.state;
    return (
      <View style = {{flex:1}}>

      <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

        <View style={{flex: 0.1, justifyContent: 'space-between', marginHorizontal: 24, flexDirection: 'row', marginTop:18}}>
          <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {this.saveDetails()}}>
            <Text style = {{fontWeight:'900', fontSize:14, lineHeight:16, color: '#4A4A4A'}}>{strings('editProfile.save')}</Text>
          </TouchableOpacity>
        </View>

        <View style = {{flex:0.2, flexDirection:'row', marginHorizontal: 24, justifyContent:'space-between', alignItems:'center'}}>

          <View style = {{flex:0.7}}>
            <Text style = {{fontWeight:'bold', fontSize:24, lineHeight: 29, color:'#4A4A4A'}}>{this.props.user.loginFieldId.name}</Text>
            <TouchableOpacity style = {{flex:0.7, justifyContent:'center', width:176, height: 32}} onPress = {() => {this.image()}}>
              <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{flex:0.55, alignItems:'center', borderRadius:4}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                <Text style = {{padding: 7, color: 'white', textAlign:'center'}}>{strings('editProfile.tap')}</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

          <View style = {{flex:0.3}}>
            <TouchableOpacity activeOpacity = {0.7} onPress = {() => {this.image()}}>
              {this.showImage()}
              <Image source = {require('./../Images/ic_cam.png')} style = {{width:20, height:16, position: 'absolute', marginTop: 30, marginLeft:28}}/>
            </TouchableOpacity>
          </View>

        </View>

        <View style = {{flex:0.05, marginHorizontal: 24, marginTop: 15}}>
          <Text style = {{fontWeight: '500', fontSize: 14, lineHeight: 16, color: '#343434'}}>{strings('editProfile.info')}</Text>
        </View>

        <View style = {{flex:0.1, marginTop: 20}}>
          <View style = {{flex: 0.90, marginHorizontal: 24}}>
            <Text style = {{fontSize:12, lineHeight:16, color : '#BABABA'}}>{strings('editProfile.name')}</Text>
            {/*<Text style = {{fontSize:14, lineHeight: 20, color : '#343434'}}>{this.props.user.userName}</Text>*/}
            <TextInput
              value = {name}
              underlineColorAndroid  = "transparent"
              style = {{fontSize:14, lineHeight: 16, color: '#343434'}}
              onChangeText = {(name) => {this.setState({name})}}
            />
          </View>
          <View style = {{borderBottomWidth:0.4, borderBottomColor:'#EBEBEB', flex:0.25}}></View>
        </View>

        <View style = {{flex:0.1, marginTop: 10}}>
          <View style = {{flex: 0.95, marginHorizontal: 24}}>
            <Text style = {{fontSize:12, lineHeight:16, color : '#BABABA'}}>{strings('editProfile.phone')}</Text>
            {/*<Text style = {{fontSize:14, lineHeight: 20, color : '#343434'}}>{phone}</Text>*/}
            <TextInput
              value = {phone}
              underlineColorAndroid  = "transparent"
              style = {{fontSize:14, lineHeight: 16, color: '#343434'}}
              onChangeText = {(phone) => {this.setState({phone})}}
            />
          </View>
          <View style = {{borderBottomWidth:0.4, borderBottomColor:'#EBEBEB', flex:0.25}}></View>
        </View>

        <View style = {{flex:0.1, marginTop: 10}}>
          <View style = {{flex: 0.95, marginHorizontal: 24}}>
            <Text style = {{fontSize:12, lineHeight:16, color : '#BABABA'}}>{strings('editProfile.email')}</Text>
            {/*<Text style = {{fontSize:14, lineHeight: 20, color : '#343434'}}>{email}</Text>*/}
            <TextInput
              value = {email}
              underlineColorAndroid  = "transparent"
              style = {{fontSize:14, lineHeight: 16, color: '#343434'}}
              keyboardType = "email-address"
              onChangeText = {(email) => {this.setState({email})}}
            />
          </View>
          <View style = {{borderBottomWidth:0.4, borderBottomColor:'#EBEBEB', flex:0.25}}></View>
        </View>

      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(editProfile);
