import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import Requests from './../Components/requests';
import Received from './../Components/received';

class orders extends Component {

  static navigatorStyle = {
    navBarHidden : true,
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  render() {
    return (
      <View style = {{flex:1}}>
        <ScrollableTabView
          style={{marginTop: 30}}
          initialPage={0}
          tabBarUnderlineStyle = {{backgroundColor : '#D8546E', height:1}}
          tabBarTextStyle = {{fontSize: 24, lineHeight: 28,fontWeight:'bold'}}
          tabBarActiveTextColor = '#BF4D73'
          tabBarInActiveTextColor = '#4A4A4A'
          prerenderingSiblingsNumber = {0}
          renderTabBar={() => <ScrollableTabBar />}
          >
          <Requests tabLabel='Requests' {...this.props}/>
          <Received tabLabel='Received' {...this.props}/>
        </ScrollableTabView>
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(orders);
