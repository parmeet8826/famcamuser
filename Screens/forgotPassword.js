import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import Validation from './../src/utils/Validation.js';

class forgotPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      phone : '',
      visible : false
    }
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  static navigatorStyle = {
    navBarHidden : true,
  }

  validationRules= () => {
      return [
          {
              field: this.state.phone,
              name: 'Phone',
              rules: 'required|no_space'
          },
      ]
  }

  back = () => {
    this.props.navigator.pop();
  }

  passwordReset = () => {
    let {phone} = this.state;

    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
    return Alert.alert(
    '',
    validaton[0],
    [
      {
        text: 'Okay',
        onPress: ()=> {

        }
      }
    ],
    { cancelable: false }
  );
}
    else {
    this.setState({visible: true});
    let number = {'phoneNumber' : phone};
    return axios.post(`${Appurl.apiUrl}forgotUser`, number)
    .then((response) => {
      return this.passwordSet(response);
    }).catch((error) => {
      if(error.response.data.success == 0) {
        Alert.alert(
            '',
            error.response.data.msg,
            [
                {
                        text: 'Okay',
                        onPress: () => {
                        this.setState({visible: false});
                } }
            ],
            { cancelable: false }
        );
      }
    })
  }
}

  passwordSet = (response) => {
    Alert.alert(
        '',
        'Link has been sent to your account',
        [
            {
                    text: 'Okay',
                    onPress: () => {
                    this.setState({visible: false});
                    this.props.navigator.push({
                      screen : 'home'
                    })
            } }
        ],
        { cancelable: false }
    );
  }

  render() {
    let {phone} = this.state;
    return (
      <View style={{flex:1, marginHorizontal: 24}}>

      <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>
        <View style={{flex:0.08, justifyContent: 'flex-start'}}>
          <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold'}}> {strings('forgotPassword.text')} </Text>
        </View>
        <View style={{flex:0.09}}>
          <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}> {strings('forgotPassword.heading')} </Text>
        </View>
        <View style = {{flex:0.15, flexDirection: 'row'}}>
          <MKTextField
            placeholder = {strings('forgotPassword.placeholder')}
            ref="phoneNumber"
            placeholderTextColor='#474D57'
            floatingLabelEnabled
            keyboardType = "numeric"
            returnKeyType = "next"
            textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
            style = {{marginTop:10, width: Dimensions.get('window').width*0.8}}
            highlightColor = '#474D57'
            tintColor = 'red'
            underlineSize={1}
            highlightColor='black'
            autoCorrect={false}
            autoCapitalize= 'none'
            onChangeText = {(phone) => {this.setState({phone})}}
          />
        </View>
        <View style = {{flex:0.1,alignItems : 'flex-end'}}>
          <TouchableOpacity activeOpacity={0.5} onPress = {() => {this.passwordReset()}}>
            <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(forgotPassword);
