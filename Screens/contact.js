import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  TextInput,
  ScrollView
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import LinearGradient from 'react-native-linear-gradient';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class contact extends Component {

  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true,
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  back = () => {
    this.props.navigator.pop();
  }

  render() {
    return (
      <View style = {{flex:1, marginHorizontal: 24}}>
        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>

        <ScrollView style = {{flex:1}} showsVerticalScrollIndicator = {false}>
          <View style = {{flex:0.1, justifyContent:'center', height: 60}}>
            <Text style = {{fontWeight:'bold', fontSize: 24, lineHeight: 24, color:'#4A4A4A'}}>Contact Us</Text>
          </View>

          <View style = {{flex:0.1, justifyContent:'center', height: 72, width:298}}>
            <Text style = {{fontSize: 14, lineHeight: 24, color : '#4A4A4A'}}>{strings('settings.contactText')}</Text>
          </View>

          <View style = {{flex:0.4, justifyContent:'flex-start', height: 120, marginTop:15, borderWidth:0.5, borderColor: '#EBEBEB'}}>
            <TextInput
              style = {{marginHorizontal: 5}}
              multiline = {true}
              underlineColorAndroid = "transparent"
              placeholder = "Enter Message"
              placeholderTextColor = "#626262"
            />
          </View>

        <TouchableOpacity activeOpacity = {0.7}>
          <LinearGradient colors = {['#8D3F7D', '#D8546E']} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }} style = {{flex:0.5, justifyContent:'center', alignItems:'center', marginTop: 180, height: 40, borderRadius: 2}}>
            <Text style = {{fontWeight:'500', fontSize: 14, lineHeight: 16, color: 'white'}}>Send</Text>
          </LinearGradient>
        </TouchableOpacity>

        </ScrollView>
      </View>
    )
  }

}


function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(contact);
