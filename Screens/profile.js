import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/EvilIcons';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import OneSignal from 'react-native-onesignal';

class profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username : '',
      profileImage : ''
    }
  }

  static navigatorStyle = {
    navBarHidden : true,
  }

  componentWillMount() {
    console.log(this.props.user.loginFieldId.id);
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  renderDetails = (data) => {
    let {username, profileImage} = this.state;
    let {actions} = this.props;
      this.setState({username : data.name, profileImage: data.Profilepicurl});
      // actions.updateUserName(data.name);
      // action.updateUserImage(data.Profilepicurl);
  }

  editProfile = () => {
    let {actions} = this.props;
    actions.toggleButton(true);

    this.props.navigator.push({
      screen : 'editProfile'
    })
  }

  logout = async ()=> {
    Alert.alert(
        '',
        'Are you sure you want to log out?',
        [
            {
                text: 'Yes', onPress: async () => {
                try{
                  OneSignal.deleteTag("phone");
                  await AsyncStorage.removeItem('user')
                  this.showHomeScreen();
                }
                catch(error){}
            }
          },
            {text: 'No', style: 'cancel'}
        ]
    );
  }

  showHomeScreen = () => {
    Navigation.startSingleScreenApp({
      screen: {
        screen : 'home'
      },
    });
  }

    termsAndConditions = () => {
      let {actions} = this.props;
      actions.toggleButton(true);

      this.props.navigator.push({
        screen : 'termsAndConditions'
      })
    }

    contact = () => {
      let {actions} = this.props;
      actions.toggleButton(true);

      this.props.navigator.push({
        screen : 'contact'
      })
    }

    showImage = () => {
      if(this.props.user.loginFieldId.image || this.props.user.profilepic) {
        return (<TouchableOpacity disabled = {this.props.user.isDisabled} style = {{marginTop:20}} activeOpacity = {0.7} onPress = {() => {this.editProfile()}}>
                  <Image source = {{uri: this.props.user.profilepic ? this.props.user.profilepic : this.props.user.loginFieldId.image}} style = {{width: 80, height: 80, borderRadius: 40}}/>
                </TouchableOpacity>)
      }
      else {
        return (<TouchableOpacity disabled = {this.props.user.isDisabled} style = {{marginTop:20}} activeOpacity = {0.7} onPress = {() => {this.editProfile()}}>
          <Icon name="camera" color='#9B9B9B' size={25} style={{height: 80, width:80, borderRadius: 40, borderWidth:0.5, borderColor: '#9B9B9B', padding:27}}/>
        </TouchableOpacity>)
      }
    }

  render() {
    let {username, profileImage} = this.state;
    return (
      <View style = {{flex:1}}>
        <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{height: 56, justifyContent:'center', alignItems:'center'}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
          <Text style = {{color: 'white', fontWeight:'bold', fontSize: 20}}>F A M C A M</Text>
        </LinearGradient>

        <View style = {{height:115, flexDirection: 'row', justifyContent:'space-between', alignItems:'center', marginHorizontal: 24}}>
          <View>
            <Text style = {{fontWeight:'bold', fontSize: 24, lineHeight: 32, color: '#4A4A4A'}}>{this.props.user.loginFieldId.name}</Text>
            <TouchableOpacity onPress = {() => {this.editProfile()}}><Text style = {{fontSize: 14, lineHeight: 16, color: '#4A4A4A', marginTop: 10}}>{strings('settings.edit')}</Text></TouchableOpacity>
          </View>

          <View>
            {this.showImage()}
          </View>
        </View>


        <View style = {{flex: 0.2, marginHorizontal: 24, justifyContent:'center'}}>
            <Text style = {{fontWeight:'bold', fontSize: 16, color : '#4A4A4A'}}>{strings('settings.text')}</Text>
        </View>

        <View style = {{flex: 0.8}}>
          <TouchableOpacity style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 16, color: '#4A4A4A'}}>{strings('settings.paymentInfo')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0.2, height:20, borderBottomColor:'#EBEBEB'}}></View>
          </TouchableOpacity>

          <TouchableOpacity disabled = {this.props.user.isDisabled} style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5} onPress = {() => {this.contact()}}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 18, color: '#4A4A4A'}}>{strings('settings.contact')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0.2, height:20, borderBottomColor:'#EBEBEB'}}></View>
          </TouchableOpacity>

          <TouchableOpacity style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 16, color: '#4A4A4A'}}>{strings('settings.suggestion')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0.2, height:20,borderBottomColor:'#EBEBEB'}}></View>
          </TouchableOpacity>

          <TouchableOpacity disabled = {this.props.user.isDisabled} style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5} onPress = {() => {this.termsAndConditions()}}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 16, color: '#4A4A4A'}}>{strings('settings.tc')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0.2, height:20, borderBottomColor:'#EBEBEB'}}></View>
          </TouchableOpacity>

          <TouchableOpacity style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 16, color: '#4A4A4A'}}>{strings('settings.lang')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0.2, height:20, borderBottomColor:'#EBEBEB'}}></View>
          </TouchableOpacity>

          <TouchableOpacity style = {{flex:0.25, justifyContent:'center'}} activeOpacity = {0.5} onPress = {() => {this.logout()}}>
            <View style = {{justifyContent:'space-between', alignItems:'center', flexDirection:'row', marginHorizontal:24}}>
                <Text style = {{fontWeight:'300', fontSize:14, lineHeight: 16, color: '#4A4A4A'}}>{strings('settings.logout')}</Text>
                <Image source = {require('./../Images/ic_dd_frwd.png')} style = {{width:28, height:14}}/>
            </View>
            <View style = {{borderBottomWidth:0, height:10}}></View>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(profile);
