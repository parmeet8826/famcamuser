import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView,
  Animated,
  AsyncStorage,
  NetInfo,
  RefreshControl
} from 'react-native';
import { strings } from '../locales/i18n';
import I18n from 'react-native-i18n';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import LinearGradient from 'react-native-linear-gradient';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from 'react-native-underline-tabbar';
import Featured from './../Components/featured';
import Comedian from './../Components/Comedian';
import OneSignal from 'react-native-onesignal';


class famcamHome extends Component {

  static navigatorStyle = {
    navBarHidden : true
  }

  constructor(props) {
    super(props);
    this.state = {
      language : '',
      featured : true,
      comedian : false,
      artists  : false,
      refreshing : false,
      result : [],
      res : [],
      all : [],
      arr : [false],
      disabled: true,
      categoryName : 'Featured',
      isSearch : false,
      searchText : '',
      searchData : []
    }
    let {actions} = this.props;
    let {arr} = this.state;
    AsyncStorage.getItem('user')
    .then((user) => {
        let details = JSON.parse(user);
        actions.getLoginUserId(details);
        console.log(details);
    });
    // AsyncStorage.getItem('image')
    // .then((image) => {
    //   let image1 = JSON.parse(image);
    //   console.log(image1, '----------------MY PROFILE IMAGE --------------------');
    //   actions.getImage(image1);
    // });
    // AsyncStorage.getItem('name')
    // .then((name) => {
    //   let name1 = JSON.parse(name);
    //   console.log(name1);
    //   actions.getUserName(name1);
    // });
    this.getCategories();
    arr[0] = true;
    //this.setState({arr});
  }

  // componentDidMount() {
  //   NetInfo.isConnected.fetch().then(isConnected => {
  //     console.log('First, is ' + (isConnected ? 'online' : 'offline'));
  //     this.setState({ internet: isConnected });
  //   });
  //   NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
  // }

  componentWillMount() {
      console.log(this.props.user.loginFieldId.id);
          OneSignal.addEventListener('received', this.onReceived);
          OneSignal.addEventListener('opened', this.onOpened);
          OneSignal.addEventListener('registered', this.onRegistered);
          OneSignal.addEventListener('ids', this.onIds);
          OneSignal.inFocusDisplaying(2);

          this.getData();
      }

      componentWillUnmount() {

        //NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);

          OneSignal.removeEventListener('received', this.onReceived);
          OneSignal.removeEventListener('opened', this.onOpened);
          OneSignal.removeEventListener('registered', this.onRegistered);
          OneSignal.removeEventListener('ids', this.onIds);
      }

      // _handleConnectionChange = (isConnected) => {
      //     console.log('internet connectton', isConnected);
      //     if(!isConnected) {
      //       Alert.alert(
      //           '',
      //           'You are not connected with Internet',
      //           [
      //               {
      //                       text: 'Okay',
      //               }
      //           ],
      //           { cancelable: false }
      //       );
      //     }
      //     this.setState({ internet: isConnected });
      // };

      onReceived(notification) {
          console.log("Notification received: ", notification);
      }

      onOpened(openResult) {
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
      }

      onRegistered(notifData) {
          console.log("Device had been registered for push notifications!", notifData);
      }

      onIds(device) {
        console.log(device)
      }

      _onRefresh() {
        this.setState({refreshing:true});
        this.getData();
      }

      getData = () => {
        let {result} = this.state;
        return axios.get(`${Appurl.apiUrl}displayAllTalentToUserAccount`)
        .then((response) => {
          console.log(response);
          this.setState({all: response.data.data});
          this.getRender(response.data.data);
        }).catch((error) => {
          console.log(error);
        })
      }


  getCategories = () => {
    return axios.get(`${Appurl.apiUrl}showProfessionsToUserDashboard`)
    .then((response) => {
      console.log(response)
      return this.showCategories(response);
    }).catch((error) => {
      console.log(error.response);
    })
  }

    showCategories = (response) => {
        let {result} = this.state;
        result.push({'category' : 'Featured'});
        response.data.professions.forEach((item, index) => {
          result.push({'category': item.professionCatagory.en});
        });
        this.setState({result});
    }

  // componentWillMount() {
  //   if(AsyncStorage.getItem('getLang')) {
  //   let selectedLang = JSON.parse(AsyncStorage.getItem('getLang'));
  //   this.setState({language : selectedLang});
  //   }
  // }



  clicked = (name, index) => {
    let {all} = this.state;
    console.log('clicked')
    let {arr,res, disabled} = this.state;
    arr = [false];
    this.setState({arr});
       if(index > -1) {
        arr[index] = true;
        this.setState({arr});
      }
      this.showDetailsOfTalent(name);
  }

showDetailsOfTalent = (name) => {
  let {all, res} = this.state;
  res.splice(0,res.length)
    this.setState({res});
  console.log(all.length);
  if(name === 'Featured') {
    this.setState({categoryName: 'Featured'});
    return this.getRender(all);
  }
  else {
    this.setState({categoryName: name});
    console.log(all.length);
  for(var i = 0; i < all.length; i++) {
    console.log(all[i].professions[0]);
    if(all[i].professions[0]==name || all[i].professions[1]==name) {
      this.getRespectiveRender(all[i]);
    }
  }
}
  // let {result, categoryName, allData} = this.state;
  // this.setState({res: []});
  // console.log(allData.professions);
  // // if(name === 'Featured') {
  // //   this.setState({categoryName: 'Featured'});
  // //   return this.getRender(allData);
  // // }
  // // else if(allData[0].professions[0] == name || allData[0].professions[1] == name) {
  // //   return this.getRender(allData);
  // // }
}

//   showDetailsOfTalent = (name) => {
//     console.log('got')
//     this.setState({res: []});
//     console.log(this.state.res);
//     if(name === 'Featured') {
//       let {result, categoryName} = this.state;
//       this.setState({categoryName: 'Featured'});
//       return fetch(`${Appurl.apiUrl}displayAllTalentToUserAccount`)
//       .then((response) => response.json())
//       .then((responseJson) => {
//         console.log(responseJson)
//         return this.getRender(responseJson.data);
//       }).catch((error) => {
//         console.log(error.response);
//       });
//     }
//     else {
//       this.setState({categoryName: name});
//     let values = {'userId' : this.props.user.loginFieldId, 'category': name}
//     console.log(values);
//     return axios.post(`${Appurl.apiUrl}filterAccordingToProfessions`, values)
//     .then((response) => {
//       return this.getRender(response.data.data);
//     }).catch((error) => {
//       console.log(error.response);
//     });
//   }
// }

  getRender = (response) => {
    let {all, res} = this.state;
    res.splice(0,res.length);
    this.setState({refreshing: false, res});
    console.log(res);
    response.forEach((item, index) => {
      res.push({id : item._id, name : item.name, profession: item.professions, pic : item.profilePicUrl});
    })
    this.setState({res});
  }

  getRespectiveRender = (response) => {
    let {res} = this.state;
    console.log(res);
    res.push({id: response._id, name: response.name, profession: response.professions, pic: response.profilePicUrl});
    this.setState({res});
  }

  talents = (talentId, related, name) => {
    let {actions} = this.props;
    actions.getTalentId(talentId);
    actions.getTalentProfession(related);
    actions.setTalentName(name);
    this.props.navigator.push({
      screen : 'talentInfo'
    })
  }

  search = (bool) => {
    this.setState({isSearch: bool});
  }

  renderAccordingly = () => {
    let {searchText} = this.state;
    if(!this.state.isSearch) {
      return (<View style = {{flex:0.1, justifyContent:'flex-end'}}><TouchableOpacity onPress = {() => {this.search(true)}}>
        <Image source = {require('./../Images/ic_search.png')} style = {{width:20, height:20}}/>
      </TouchableOpacity></View>
      );
    }
    else {
      return (
        <View style = {{flex:0.8, width:Dimensions.get('window').width, justifyContent:'flex-end'}}>
          <TextInput
          placeholder = "Search"
          placeholderTextColor = '#AAAFB9'
          underlineColorAndroid = 'transparent'
          style = {{backgroundColor:'white', borderRadius:15, height:40}}
          autoFocus = {true}
          onChangeText = {(searchText) => {this.searchTextFunc(searchText)}}
          />
          <TouchableOpacity style = {{position:'absolute', bottom:12, left:150}} hitSlop = {{top:7, right:7, bottom:7, left:7}} onPress = {() => {this.search(false)}}>
            <Icon name = "close" size = {15} style = {{width:15}}/>
          </TouchableOpacity>
        </View>
      )
    }
  }


  searchTextFunc = (val) => {
    let {res} = this.state;
    this.setState({searchText: val});
    let x = res.filter((item) => {
      return item.name.toLowerCase().match(val.toLowerCase());
    })
    this.setState({searchData: x});
  }

  renderActualData = (value,index)  => {
    return <View key = {index} style = {{flex:0.2, marginTop: 5, marginBottom: 10, marginHorizontal:14, borderRadius: 6}}>
    <TouchableOpacity style={{flex:1, borderRadius: 60}} activeOpacity={0.9} onPress = {() => {this.talents(value.id, value.profession[0], value.name)}}>
      <LinearGradient style = {{flex:1, marginHorizontal: 2, borderRadius:6}} colors = {['black', 'black']}>
        <Image resizeMode='cover' source = {{uri : value.pic}} style = {{width: 328, height: 152, opacity: 0.75,alignSelf: 'center', borderRadius: 6}}/>
      </LinearGradient>
      <View style = {{flex:0.9,justifyContent:'flex-end', marginHorizontal: 16, position: 'absolute', top:0, bottom: 16, left: 0, right:0}}>
        <Text style = {{color: 'white', fontSize: 20, fontWeight:'bold'}}>{value.name}</Text>
        <Text style = {{color: 'white'}}>{value.profession[0]} {value.profession[1]}</Text>
      </View>
      </TouchableOpacity>
    </View>
  }


  renderSearchedData = (value,index) => {
    return <View key = {index} style = {{flex:0.2, marginTop: 5, marginBottom: 10, marginHorizontal:14, borderRadius: 6}}>
    <TouchableOpacity style={{flex:1, borderRadius: 60}} activeOpacity={0.9} onPress = {() => {this.talents(value.id, value.profession[0], value.name)}}>
      <LinearGradient style = {{flex:1, marginHorizontal: 2, borderRadius:6}} colors = {['black', 'black']}>
        <Image resizeMode='cover' source = {{uri : value.pic}} style = {{width: 328, height: 152, opacity: 0.75,alignSelf: 'center', borderRadius: 6}}/>
      </LinearGradient>
      <View style = {{flex:0.9,justifyContent:'flex-end', marginHorizontal: 16, position: 'absolute', top:0, bottom: 16, left: 0, right:0}}>
        <Text style = {{color: 'white', fontSize: 20, fontWeight:'bold'}}>{value.name}</Text>
        <Text style = {{color: 'white'}}>{value.profession[0]} {value.profession[1]}</Text>
      </View>
      </TouchableOpacity>
    </View>
  }

  renderAllData = (field) => {
    let {res, searchData} = this.state;
    if(field == '') {
      return res.map((value, index) => { return this.renderActualData(value,index)});
    }
    else if(searchData.length == 0) {
      return <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center', marginTop:20}}><Text style={{fontSize:20}}>No Data Found</Text></View>
    }
    else {
      return searchData.map((value,index) => { return this.renderSearchedData(value,index)})
    }
  }


  render() {
    let {actions} = this.props;
    console.log(this.props.user.loginFieldId);
    let {language, featured, comedian, artists, result, arr, res, categoryName, all, isSearch} = this.state;
    I18n.locale = language;
    I18n.currentLocale();
    return (
      <View style = {{flex:1}}>
        <View style = {{flex:0.25}}>
          <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{flex:1}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
            <View style = {{flex:1, maxHeight:50, marginTop: 10, flexDirection:'row'}}>
              <View style = {{flex:0.9, justifyContent:'center'}}>
                <Text style = {{fontWeight:'bold', fontSize:20, color: 'white', textAlign:'center', marginTop:10, backgroundColor: 'transparent'}}>F A M C A M</Text>
              </View>


                {this.renderAccordingly()}


            </View>
            <View style={{flex:0.1}}>
              <ScrollView horizontal = {true} showsHorizontalScrollIndicator = {false} style = {{flexDirection : 'row'}}>
                {
                  result ? result.map((value,index) => {
                    return <View key = {index} style = {{justifyContent:'center', alignItems:'center', marginLeft: 4}}>
                    <TouchableOpacity disabled = {arr[index] ? true : false} hitSlop = {{top:5, left:5, bottom:5, right:5}} onPress = {() => {this.clicked(value.category, index)}} style = {{justifyContent:'center', alignItems:'center', backgroundColor : arr[index] ? 'white' : null, width: 80 , height : arr[index] ? 32 : null, borderRadius : arr[index] ? 100 : null}}>
                      <Text style = {{color : arr[index] ? '#90407C' : 'white', fontSize : 14, lineHeight:16, fontWeight:'bold'}}>{value.category}</Text>
                      </TouchableOpacity>
                    </View>
                  })
                  :null
                }
              </ScrollView>
              </View>
            </LinearGradient>
            </View>

            <View style = {{flex:0.8}}>
            <ScrollView showsVerticalScrollIndicator = {false}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh.bind(this)}
                  colors={['#BF4D73', '#D8546E', '#8D3F7D']}
                  title = "Loading"
                />
              }
            >
              <View style = {{flex:0.05, marginHorizontal: 14, justifyContent:'center', marginTop:15}}>
                <Text style = {{fontSize:22, lineHeight:24, fontWeight:'bold', color:'#4A4A4A'}}>{categoryName}</Text>
              </View>
              {
                this.renderAllData(this.state.searchText)
              }
            </ScrollView>
            </View>

        </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(famcamHome);
