import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
  Picker,
  ScrollView
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import { Dropdown } from 'react-native-material-dropdown';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


class payment extends Component {

  constructor(props) {
    super(props);
    this.state = {
      credit : true,
      debit: false,
      year: [],
      month : [{
          value : '01'
      },
      {
        value : '02'
      },
      {
        value : '03'
      },
      {
        value : '04'
      },
      {
        value : '05'
      },
      {
        value : '06'
      },
      {
        value : '07'
      },
      {
        value : '08'
      },
      {
        value : '09'
      },
      {
        value : '10'
      },
      {
        value : '11'
      },
      {
        value : '12'
      },
    ]
    }
    this.getYears();
  }

  static navigatorStyle = {
    navBarHidden: true
  }

  getYears = () => {
    let {year} = this.state;
    let date = new Date();
    for(let i = 0; i < 20; i++) {
      year.push({value : date.getFullYear()+i});
    }
    this.setState({year});
  }

  back = () => {
    this.props.navigator.pop();
  }

  changeState = () => {
    let {credit, debit} = this.state;
    if(credit) {
      this.setState({credit: false, debit: true});
    }
    else {
      this.setState({debit: false, credit: true});
    }
  }

  render() {
    let {credit, debit} = this.state;
    return (
      <View style = {{flex:1, marginHorizontal: 24}}>
        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity  hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>

        <View style = {{flex:0.1}}>
          <Text style = {{fontSize:24, lineHeight: 28, fontWeight:'bold', color : '#4A4A4A'}}>{strings('payment.paymentOptions')}</Text>
        </View>

        <View style = {{flex:0.2}}>
          <View style = {{flex:1, flexDirection:'row', justifyContent:'flex-start'}}>
            <TouchableOpacity onPress = {() => {this.changeState()}}>
              {credit ? <Image source = {require('./../Images/ic_option_color.png')} style = {{width:22, height:22}}/> : <Image source = {require('./../Images/ic_option.png')} style = {{width:22, height:22}}/>}
            </TouchableOpacity>

            <View style = {{flexDirection:'row', justifyContent:'space-between', flex:1, marginLeft:20}}>
              <View>
                <Text style = {{fontSize:14, lineHeight:16, color : '#343434'}}>Credit Card</Text>
                <Text style = {{fontSize:12, lineHeight:16, color : '#BABABA'}}>VISA (5547)</Text>
              </View>
                <TouchableOpacity style = {{borderColor : '#979797', borderRadius: 3, borderType: 'solid', width: 64, height: 24, borderWidth:1, justifyContent:'center', alignItems : 'center'}}>
                  <Text style = {{fontSize:10, lineHeight:16}}>{strings('payment.change')}</Text>
                </TouchableOpacity>
            </View>

          </View>

          <View style = {{flex:1, flexDirection:'row', justifyContent:'flex-start'}}>
            <TouchableOpacity onPress = {() => {this.changeState()}}>
              {debit ? <Image source = {require('./../Images/ic_option_color.png')} style = {{width:22, height:22}}/> : <Image source = {require('./../Images/ic_option.png')} style = {{width:22, height:22}}/>}
            </TouchableOpacity>

            <View style = {{flexDirection:'row', justifyContent:'space-between', flex:1, marginLeft:20}}>
              <View>
                <Text style = {{fontSize:14, lineHeight:16, color : '#343434'}}>Debit Card</Text>
                <Text style = {{fontSize:12, lineHeight:16, color : '#BABABA'}}>MASTER (4711)</Text>
              </View>
                <TouchableOpacity style = {{borderColor : '#979797', borderRadius: 3, borderType: 'solid', width: 64, height: 24, borderWidth:1, justifyContent:'center', alignItems : 'center'}}>
                  <Text style = {{fontSize:10, lineHeight:16}}>{strings('payment.change')}</Text>
                </TouchableOpacity>
            </View>

          </View>

        </View>

        <View style = {{flex:0.06, borderBottomWidth:0.5, borderBottomColor : '#ACACAC'}}>
          <Text style = {{fontSize:16, lineHeight:24, fontWeight:'bold', color : '#4A4A4A'}}>{strings('payment.newcard')}</Text>
        </View>

        <View style = {{flex:0.1, borderWidth: 1, borderColor: '#E0E0E0', borderType: 'solid', top:20, borderRadius:2}}>
          <View style = {{flex:0.2}}><Text style = {{margin:5, fontSize:12, lineHeight: 16, color : '#BABABA'}}>{strings('payment.cardno')}</Text></View>
          <View style = {{flex:0.8, justifyContent:'center'}}>
          <TextInput
            placeholder = "xxxx-xxxx-xxxx-xxxx"
            placeholderTextColor = "#9B9B9B"
            style = {{fontSize: 14, fontWeight : '600'}}
            underlineColorAndroid = "transparent"
          />
          </View>

        </View>

        <View style = {{flex:0.05}}></View>

        <View style = {{flex:0.13, flexDirection:'row'}}>
          <View style = {{flex:0.45, borderColor:'#E0E0E0', borderWidth:1}}>
            <View style = {{flex:0.3, justifyContent:'flex-end'}}>
              <Text style = {{marginLeft: 10}}>Month</Text>
            </View>
            <View style = {{flex: 0.7, justifyContent:'flex-start'}}>
              <Dropdown
                containerStyle = {{flex:1, marginTop: -10, marginLeft: 10}}
                label={strings('payment.month')}
                data={this.state.month}
                labelFontSize = {12}
              />
            </View>
          </View>
          <View style = {{flex:0.1}}></View>
            <View style = {{flex:0.45, borderColor:'#E0E0E0', borderWidth:1}}>
            <View style = {{flex:0.3, justifyContent:'flex-end'}}>
              <Text style = {{marginLeft: 10}}>Year</Text>
            </View>
            <View style = {{flex: 0.7}}>
              <Dropdown
                containerStyle = {{flex:1, marginTop: -10, marginLeft:10}}
                label={strings('payment.year')}
                data={this.state.year}
                labelFontSize = {12}
              />
            </View>
          </View>
        </View>

        <View style = {{flex: 0.11, flexDirection:'row', marginTop:7}}>
          <View style = {{flex:0.4, borderColor: '#E0E0E0', borderWidth:1}}>
            <View style = {{flex:0.3, justifyContent:'flex-end'}}>
              <Text style = {{marginLeft: 10}}>{strings('payment.cvv')}</Text>
            </View>

            <View style = {{flex:0.7, justifyContent: 'center'}}>
              <TextInput
                placeholder = "Enter CVV"
                maxLength = {3}
                secureTextEntry = {true}
                keyboardType = "numeric"
                placeholderTextColor = "#9B9B9B"
                style = {{fontSize: 14, fontWeight : '600', marginLeft:10}}
                underlineColorAndroid = "transparent"
              />
            </View>

          </View>
        </View>

        <TouchableOpacity style = {{flex:0.1, marginTop:20}} activeOpacity = {0.8}>
          <LinearGradient colors={['#8D3F7D', '#D8546E']} style = {{flex:1, alignItems:'center', borderRadius:2, justifyContent:'center', alignItems:'center'}} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
            <Text style = {{color : 'white', fontSize:14, lineHeight:16, fontWeight:'500'}}>{strings('payment.pay')} SAR {this.props.user.price}</Text>
          </LinearGradient>
        </TouchableOpacity>

      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(payment);
