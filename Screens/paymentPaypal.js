import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  Text
} from 'react-native';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import Appurl from './../config';
import Spinner from 'react-native-loading-spinner-overlay';

var BTClient = require('react-native-braintree-xplat');

const card = {
  number : "4111111111111111",
  expirationDate : "10/20",
  cvv : "400",
}

const options = {
  bgColor: '#FFF',
  tintColor: 'orange',
  callToActionText: 'Save',
  threeDSecure: {
    amount: 1.0
  }
}
class paymentPaypal extends Component {

  static navigatorStyle = {
    navBarHidden: true,
    tabBarHidden: true
  }

  constructor(props) {
    super(props);
    this.state = {
      visible : false
    }
  }

  componentWillMount() {
    BTClient.setup(this.props.user.token);
  }
  abc = () => {
    this.setState({visible: true});
    BTClient.getCardNonce(card).then(function(nonce) {
    console.log(nonce);
    let values = {'ammount': 50, 'nonce': nonce};
    return axios.post(`${Appurl.apiUrl}paymentApi`,values)
    .then((response) => {
      console.log(response);
      return this.getResponse(response);
    })
})
.catch(function(err) {
  console.log(err);
});
  }

  getResponse = (response) => {
    this.setState({visible: false});
  }

  render() {
  return (
    <View style = {{flex:1, margin:20}}>

      <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

      <TouchableOpacity onPress = {() => {this.abc()}}>
        <Text>Hello</Text>
      </TouchableOpacity>
    </View>
  )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(paymentPaypal);
