import React,{ Component } from 'react';
import { Text,View,ImageBackground,Alert,TouchableOpacity,StyleSheet,Dimensions,Image,AsyncStorage, NetInfo } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/EvilIcons';
import I18n from 'react-native-i18n';
import { strings } from '../locales/i18n';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Auth0 from 'react-native-auth0';
import OneSignal from 'react-native-onesignal';
import SplashScreen from 'react-native-splash-screen';

var credentials = require('../src/auth0-credentials');
const auth0 = new Auth0(credentials);

class home extends Component {
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      isSwitch1On: false,
      langColor : true,
    }
    this.saveLanguage('en');
  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({ internet: isConnected });
    });
    NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
    SplashScreen.hide();
  }

  componentWillMount() {
          OneSignal.addEventListener('received', this.onReceived);
          OneSignal.addEventListener('opened', this.onOpened);
          OneSignal.addEventListener('registered', this.onRegistered);
          OneSignal.addEventListener('ids', this.onIds);
          OneSignal.inFocusDisplaying(2);
      }

      componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);
          OneSignal.removeEventListener('received', this.onReceived);
          OneSignal.removeEventListener('opened', this.onOpened);
          OneSignal.removeEventListener('registered', this.onRegistered);
          OneSignal.removeEventListener('ids', this.onIds);
      }

      onReceived(notification) {
          console.log("Notification received: ", notification);
      }

      onOpened(openResult) {
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
      }

      onRegistered(notifData) {
          console.log("Device had been registered for push notifications!", notifData);
      }

      onIds(device) {
        console.log(device)
      }

      _handleConnectionChange = (isConnected) => {
          console.log('internet connectton', isConnected);
          if(!isConnected) {
            Alert.alert(
                '',
                'You are not connected with Internet',
                [
                    {
                            text: 'Okay',
                    }
                ],
                { cancelable: false }
            );
          }
          this.setState({ internet: isConnected });
      };

      registerScreen = () => {
        let {actions} = this.props;
        actions.toggleButton(true);

        this.props.navigator.push({
          screen : 'register',
        })
      }

      appLang = async (selectedLg)=> {
    let { langColor } = this.state;
    if(selectedLg === 'en') {
      this.setState({langColor:true});
      I18n.locale = 'en';
      I18n.currentLocale();
    }
    else {
      this.setState({langColor:false});
      I18n.locale = 'ar';
      I18n.currentLocale();
    }
    this.saveLanguage(selectedLg);
  }

  saveLanguage = async(language) => {
    await AsyncStorage.setItem('getLang', language);
  }

  loginScreen = () => {
    let {actions} = this.props;
    actions.toggleButton(true);

    this.props.navigator.push({
      screen : 'login'
    })
  }

  login = () => {
    auth0
    .webAuth
    .authorize({
      scope: 'openid profile email',
    connection : 'facebook',
    audience: 'https://' + credentials.domain + '/userinfo'})
    .then(credentials =>
      console.log(credentials)
      // Successfully authenticated
      // Store the accessToken
    )
    .catch(error => console.log(error));
  }




  render() {
    let { langColor } = this.state;
    return (
      <View style={{flex:1}}>
        <ImageBackground source={require('./../Images/pexels-photo-69970.png')} style={styles.backgroundImage}>
          <View style={{flex:0.1, flexDirection: 'row', marginHorizontal: 15, alignItems: 'flex-end'}}>
            <View style={{flex:0.35, height:32, flexDirection: 'row'}}>
              <TouchableOpacity activeOpacity={1} style={{flex:0.5, backgroundColor: langColor?'white':'#EDEDED', borderTopLeftRadius: 12, borderBottomLeftRadius: 12, justifyContent: 'center'}} onPress={()=>this.appLang('en')}>
                <Text style={{textAlign: 'center', color: langColor?'#4A4A4A':'#BFBFBF', fontWeight: '600', fontSize: 14, lineHeight: 16}}>Eng</Text>
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={1} style={{flex:0.5, backgroundColor: langColor?'#EDEDED':'white', borderTopRightRadius: 12, borderBottomRightRadius: 12, justifyContent: 'center'}} onPress={()=>this.appLang('ar')}>
                <Text style={{fontFamily: 'GeezaPro',textAlign: 'center', color : langColor?'#BFBFBF':'#4A4A4A', fontWeight: 'bold', fontSize: 14, lineHeight: 16}}>ع</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: langColor?0.45:0.25}}></View>
            <TouchableOpacity disabled={this.props.user.isDisabled} hitSlop={{top:7, bottom:7, left:7, right:7}} style={{flex: langColor?0.2:0.4,height: 32, justifyContent: 'center'}} onPress = {() => {this.loginScreen()}}>
              <Text style={{color: 'white', textAlign: 'right', fontSize: 18, lineHeight: 20, fontWeight: 'bold'}}>{strings('home.login')}</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.25}}></View>
          <View style={{flex:0.25,justifyContent: 'center', alignItems: 'center'}}>
            <Image style={{height: 120, width: 177}} source={require('./../Images/FAMCAMM3.png')}/>
          </View>
          <View style={{flex:0.15}}></View>
          <View style={styles.social}>
            <TouchableOpacity style={styles.btnFb} onPress = {() => this.login()}>
              <View style={{flex:0.2, alignItems: 'center'}}>
                <Icon name="sc-facebook" size={25} color="white" style={{width:25}}/>
              </View>
              <View style={{flex:0.8}}>
                <Text style={{textAlign: langColor?null:'center',color:'#FFFFFF',fontSize:12, fontWeight: 'bold', lineHeight: 16}}>{strings('home.fb_button')}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity disabled={this.props.user.isDisabled} style={styles.btnEmail} onPress = {() => this.registerScreen()}>
              <View style={{flex:0.2}}></View>
              <View style={{flex:0.8}}>
                <Text style={{textAlign: langColor?null:'center',color:'#18181A',fontSize:12, fontWeight: 'bold', lineHeight: 16}}>{strings('home.email')}</Text>
              </View>
            </TouchableOpacity>
            <View style={{flex:1/3, backgroundColor: 'transparent'}}>
              <Text style={{color: '#FFFFFF', fontSize: 12, lineHeight: 14, opacity: 0.88}}>{strings('home.message')}</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  backgroundImage: {
        flex: 1,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
  },
  social: {
    flex:0.3,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  btnFb: {
    height:50,
    width:Dimensions.get('window').width*0.8,
    flexDirection:'row',
    alignItems:'center',
    backgroundColor:'#3B5998',
    borderWidth:0,
    borderRadius:25
  },
  btnEmail: {
    height:50,
    width:Dimensions.get('window').width*0.8,
    flexDirection:'row',
    alignItems:'center',
    backgroundColor:'#FFFFFF',
    borderWidth:0,
    borderRadius:25
  },
})

function mapStateToProps(state, ownProps) {
  return {
      user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(userActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(home);
