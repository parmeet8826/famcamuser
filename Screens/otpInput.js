import React, { Component } from 'react';
import { Platform,Alert,TextInput,StyleSheet,Text,View,ImageBackground,Dimensions,Image,TouchableOpacity } from 'react-native';
import { strings } from '../locales/i18n';
import Icon from 'react-native-vector-icons/Ionicons';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';

class otpInput extends Component {
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      one: '',
      two: '',
      three: '',
      four: '',
      isOneValid: false,
      isTwoValid: false,
      isThreeValid: false,
      isFourValid: false,
      visible : false,
      empty_character : ' '
    }
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  back = ()=> {
    this.props.navigator.pop();
  }
  resendOtp = ()=> {
    this.setState({visible:true});
    let values = {'userId' : this.props.user.loginFieldId};
    return axios.post(`${Appurl.apiUrl}resendUserotp`, values)
    .then((response) => {
      this.setState({visible: false});
      alert('OTP sent successfully')
    }).catch((error) => {
      if(error.response.data.success == 0) {
        Alert.alert(
            '',
            error.response.data.msg,
            [
                {
                        text: 'Okay',
                        onPress: () => {
                        this.setState({visible: false});
                } }
            ],
            { cancelable: false }
        );
      }
    })
  }
  validate = (field, value) => {
    let {actions} = this.props;
    let {email,callingCode,phone} = this.state;
    this.setState({[field]: value});
    var Regex = /^[0-9]$/;
    switch(field) {
      case 'one' : {
        if(!value.match(Regex)) {
          this.setState({isOneValid : true});
        }
        else {
          this.setState({isOneValid : false});
          this.setState({one: value})
        }
        break;
      }
      case 'two' : {
        if(!value.match(Regex)) {
          this.setState({isTwoValid : true});
        }
        else {
          this.setState({isTwoValid : false});
          this.setState({two: value})
        }
        break;
      }
      case 'three' : {
        if(!value.match(Regex)) {
          this.setState({isThreeValid : true});
        }
        else {
          this.setState({isThreeValid : false});
          this.setState({three: value})
        }
        break;
      }
      case 'four' : {
        if(!value.match(Regex)) {
          this.setState({isFourValid : true});
        }
        else {
          this.setState({isFourValid : false});
          this.setState({four: value})
        }
        break;
      }
      case 'default' : {
        alert('Invalid');
        break;
      }
    }
  }
  register2 = ()=> {
    let {one, two, three, four, isOneValid, isTwoValid, isThreeValid, isFourValid, visible} = this.state;
    if(!one || !two || !three || !four) {
      alert('OTP is of 4 digits!')
    }
    else if(!isOneValid && !isTwoValid && !isThreeValid && !isFourValid) {
      this.setState({visible: true})
      let otp = one+two+three+four;
      let values = {'otp' : otp, 'userId' : this.props.user.loginFieldId}
      return axios.post(`${Appurl.apiUrl}verifyUserotp`, values)
      .then((response) => {
        return this.verifyotp(response, values);
      }).catch((error) => {
        if(error.response.data.msg == 'Invalid otp') {
          Alert.alert(
              '',
              'The code you entered is incorrect. Please check the code and try again.',
              [
                  {
                          text: 'Okay',
                          onPress: () => {
                          this.setState({visible: false});
                  } }
              ],
              { cancelable: false }
          );
        }
      })
    }
    else {
      Alert.alert(
          '',
          'OTP is incorrect',
          [
              {
                      text: 'Okay',
                      onPress: () => {
                      this.setState({visible: false});
              } }
          ],
          { cancelable: false }
      );
    }
  }
  verifyotp = (response, values) => {

    Alert.alert(
        '',
        'OTP Verified successfully',
        [
            {
                    text: 'Okay',
                    onPress: () => {
                    this.setState({visible: false});
                    this.props.navigator.push({
                      screen: 'profileSetup'
                    })
            } }
        ],
        { cancelable: false }
    );
  }


    render() {
      let {email, phone, visible} = this.state;
      return (
        <View style={{flex:1, marginHorizontal: 24}}>
          <Spinner visible={visible} cancelable={false} textStyle={{color: '#FFF'}} />
          <View style={{flex: 0.1, justifyContent: 'center'}}>
            <TouchableOpacity hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
              <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.08, justifyContent: 'flex-start'}}>
            <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold' , color: '#000000'}}>{strings('otp.otp')}</Text>
          </View>
          <View style={{flex:0.09}}>
            <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}>{strings('otp.heading')}</Text>
            <Text style = {{fontSize: 14, lineHeight: 20, color : '#000000', fontWeight:'bold'}}>{this.props.user.code}-{this.props.user.phone}</Text>
          </View>
          <View style = {{flex:0.15, flexDirection: 'row', justifyContent: 'space-around',}}>
            <TextInput
              style={{width:48,borderBottomColor : '#E9EAED', borderBottomWidth: 2, fontSize: 40, width:48, textAlign:'center'}}
              ref="first"
              maxLength = {1}
              underlineColorAndroid='transparent'
              returnKeyType="next"
              keyboardType= 'numeric'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText={(one) => {this.validate('one',one)
              if(one.length > 0) {
                this.refs.second.focus();
              }
            }
          }
            />
            <TextInput
              style={{width:48,borderBottomColor : '#E9EAED', borderBottomWidth: 2, fontSize: 40, width:48, textAlign:'center'}}
              ref="second"
              maxLength = {1}
              underlineColorAndroid='transparent'
              returnKeyType="next"
              keyboardType= 'numeric'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText = {(two) => {this.validate('two', two)
              if(two.length == 0 || !two) {
                this.refs.first.focus();
              }
                else if(two.length > 0 && two) {
                  this.refs.third.focus();
                }
            }
          }
            />
            <TextInput
              style={{width:48,borderBottomColor : '#E9EAED', borderBottomWidth: 2, fontSize: 40, width:48, textAlign:'center'}}
              ref="third"
              maxLength = {1}
              underlineColorAndroid='transparent'
              returnKeyType="next"
              keyboardType= 'numeric'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText={(three) => {this.validate('three',three)
              if(three.length == 0 || !three) {
                this.refs.second.focus();
              }
                else if(three.length > 0 && three) {
                  this.refs.forth.focus();
                }
            }
          }
            />
            <TextInput
              style={{width:48,borderBottomColor : '#E9EAED', borderBottomWidth: 2, fontSize: 40, width:48, textAlign:'center'}}
              ref="forth"
              maxLength = {1}
              underlineColorAndroid='transparent'
              returnKeyType="done"
              keyboardType= 'numeric'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText={(four) => {this.validate('four',four)
              if(four.length == 0) {
                this.refs.third.focus();
              }
            }
          }
            />
          </View>
          <View style={{flex:0.1, justifyContent: 'center',alignItems: 'flex-end', marginRight: 15}}>
            <TouchableOpacity style={{width:86}} onPress={() => {this.resendOtp()}}>
              <Text style={{fontSize: 14, lineHeight: 16, color: '#BF4D73', fontWeight: 'bold', textAlign: 'center'}}>{strings('otp.resendOtp')}</Text>
            </TouchableOpacity>
          </View>
          <View style = {{flex:0.1,alignItems : 'flex-end'}}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => {this.register2()}}>
              <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
            </TouchableOpacity>
          </View>
        </View>
      )
    }
}

function mapStateToProps(state, ownProps) {
  return {
      user: state.user
  };
}
function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(userActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(otpInput);
