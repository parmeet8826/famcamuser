import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
  Picker
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import Validation from './../src/utils/Validation.js';
import OneSignal from 'react-native-onesignal';

var play='';
class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email : '',
      phone : '',
      isEmailValid : false,
      isPhoneValid : false,
      visible : false,
      pickerData : null,
      countryCode : '+966',
      password: '',
      show_password: true,
    }
    console.log(Appurl.apiUrl);
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  componentWillMount() {
      OneSignal.addEventListener('ids', this.onIds);
  }
  onIds(device) {
    play=device.userId;
  }

  validationRules= () => {
      return [
          {
              field: this.state.email,
              name: 'Email Id',
              rules: 'required|email|max:100|no_space'
          },
          {
              field: this.state.password,
              name: 'Password',
              rules: 'required|no_space|min:6|max:16'
          },
          {
            field: this.state.phone,
            name : 'Phone',
            rules: 'required|no_space'
          }

      ]
  }

  static navigatorStyle = {
    navBarHidden : true,
  }

  back = () => {
    this.props.navigator.pop()
  }

  checkEmail = () => {
    let {isEmailValid} = this.state;
    if(isEmailValid) {
      return <Text style = {{color:'#C2567A', marginTop: 10}}>Email ID is not Correct!</Text>
    }
  }

  checkPhone = () => {
    let {isPhoneValid} = this.state;
    if(isPhoneValid) {
      return <Text style = {{color: '#C2567A', marginTop:10}}>Phone Number should be correct!</Text>
    }
  }

  showPassword = () => {
    let {show_password} = this.state;
    if(show_password) {
      this.setState({show_password:false})
    }
    else {
      this.setState({show_password:true})
    }
  }

  otpVerfiication = () => {
    let {actions} = this.props;
    let {isEmailValid, isPhoneValid, email, phone, visible, countryCode, password} = this.state;

    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
      return Alert.alert(
        '',
        validaton[0],
        [
          {
            text: 'Okay',
            onPress: ()=> {

            }
          }
        ],
        { cancelable: false }
      );
    }

    else {
      console.log(countryCode);
      this.setState({visible: true})
      actions.getEmail(email);
      actions.getPhone(phone);
      actions.getCountryCode(countryCode);
      OneSignal.sendTag("phone", email);
      let values = {'email' : email, 'phoneNumber': phone, 'callingCode' : countryCode , 'deviceType' : (Platform.OS == 'ios') ? 'IOS' : 'ANDROID', 'password' : password}
      return axios.post(`${Appurl.apiUrl}userRegister`, values)
      .then((response) => {
        return this.getData(response, values);
      }).catch((error) => {
        if(error.response.data.success == 0) {
          Alert.alert(
              '',
              error.response.data.msg,
              [
                  {
                          text: 'Okay',
                          onPress: () => {
                          this.setState({visible: false});
                  } }
              ],
              { cancelable: false }
          );
        }
      })
  }
  }

  getData = (response, values) => {
    console.log(response);
    let {visible} = this.state;
    let {actions} = this.props;
    this.setState({visible: false});
    let userid = response.data.userId;
    console.log(userid);
    actions.getLoginUserId(userid);
    this.props.navigator.push({
      screen : 'otpInput'
    })
  }

  func = (item, index) => {
    this.setState({countryCode: item});
  }

    render() {
      let {email, phone, countryCode, password, show_password} = this.state;
      console.log(countryCode);
      return (
        <View style = {{flex:1, marginHorizontal: 24}}>

        <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

          <View style={{flex: 0.1, justifyContent: 'center'}}>
            <TouchableOpacity  hitSlop = {{top:7, left:7, bottom:7, right:7}} style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
              <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
            </TouchableOpacity>
          </View>
          <View style={{flex:0.08, justifyContent: 'flex-start'}}>
            <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold', color: '#000000'}}>{strings('register.register')}</Text>
          </View>
          <View style={{flex:0.07}}>
            <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}>{strings('register.heading')}</Text>
          </View>
          <View style={{flex:0.35}}>
            <View style = {{flex:1/3}}>
                <MKTextField
                  placeholder = {strings('register.placeholderEmail')}
                  ref="emailPhone"
                  placeholderTextColor='#AAAFB9'
                  floatingLabelEnabled
                  keyboardType = "email-address"
                  returnKeyType = "next"
                  textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
                  style = {{marginTop:10}}
                  underlineSize={1}
                  highlightColor='#474D57'
                  tintColor='#C2567A'
                  autoCorrect={false}
                  autoCapitalize= 'none'
                  onChangeText = {(email) => {this.setState({email})}}
                  onSubmitEditing = {(event) => {this.refs.password.focus()}}
                />
            </View>

            <View style = {{flex:1/3, flexDirection: 'row'}}>
              <MKTextField
                placeholder = {strings('register.placeholderPassword')}
                ref="password"
                placeholderTextColor='#AAAFB9'
                floatingLabelEnabled
                password={show_password}
                keyboardType = "default"
                returnKeyType = "next"
                textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
                style = {{flex:0.93, marginTop: 10}}
                underlineSize={1}
                highlightColor='#474D57'
                tintColor='#C2567A'
                autoCorrect={false}
                autoCapitalize= 'none'
                onChangeText = {(password) => {this.setState({password})}}
                onSubmitEditing = {(event) => {this.refs.phone.focus()}}
              />
              <TouchableOpacity style={{flex:0.07,justifyContent: 'flex-end', height: 15, alignSelf: 'center', marginBottom: -25}} onPress={() => {this.showPassword()}}>
                <Image source={show_password ? require('./../Images/ic_eye.png') : require('./../Images/GroupN.png')} style={{tintColor: '#000000',height: 17, width: 20}}/>
              </TouchableOpacity>
            </View>

            <View style = {{flex:1/3, flexDirection:'row'}}>
              <View style={{flex: 0.15}}>
                <Picker
                  selectedValue={this.state.countryCode}
                  mode='dropdown'
                  style={{width:110, marginTop: 20}}
                  onValueChange = {(itemValue, itemIndex) => {this.func(itemValue, itemIndex)}}>
                  <Picker.Item label="+966" value="+966" />
                  <Picker.Item label="+91" value="+91" />
                </Picker>
               </View>
                <View style={{flex:0.77}}>
              <MKTextField
                placeholder = {strings('register.placeholderNumber')}
                ref="phone"
                placeholderTextColor='#AAAFB9'
                floatingLabelEnabled
                keyboardType = "phone-pad"
                returnKeyType = "done"
                textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
                style = {{marginTop:10, width:Dimensions.get('window').width-160, marginLeft: 50}}
                underlineSize={1}
                highlightColor='#474D57'
                tintColor='#C2567A'
                autoCorrect={false}
                autoCapitalize= 'none'
                onChangeText = {(phone) => this.setState({phone})}
              />
              </View>
            </View>
            </View>
            <View style={{flex:0.05}}></View>
          <View style = {{flex:0.1,alignItems : 'flex-end'}}>
            <TouchableOpacity activeOpacity={0.5} onPress = {() => {this.otpVerfiication()}}>
              <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
            </TouchableOpacity>
          </View>

        </View>
      )
    }
}

const styles = StyleSheet.create({
  backButton : {
    margin: 20,
    maxHeight:10
  },

  registerHeading : {
    marginLeft: 20,
    marginTop:15
  },

  text : {
    margin:20,
    maxHeight:10
  },

  emailTextField : {
    margin:20,
    backgroundColor : '#C2567A'
  },

  phoneNumberTextField : {
    marginHorizontal: 20,
    backgroundColor : 'green',
    justifyContent : 'flex-start',
    alignItems : 'flex-start',
  },

  passwordTextField : {
    marginHorizontal: 20,
    backgroundColor : 'black',
    justifyContent : 'flex-start',
    alignItems : 'flex-start'
  },

  button : {
    flexDirection : 'row',
    justifyContent : 'flex-end',
    alignItems : 'flex-end',
    marginRight: 20,

  }

})

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(Register);
