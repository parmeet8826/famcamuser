import React, { Component } from 'react';
import { Platform,Alert,StyleSheet,Text,View,ImageBackground,Dimensions,Image,TouchableOpacity, AsyncStorage, Linking, PermissionsAndroid, TextInput } from 'react-native';
import { strings } from '../locales/i18n';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';
import { RNS3 } from 'react-native-aws3';
import Validation from './../src/utils/Validation.js';

const abc='';
const bca='';
const option = {
  keyPrefix: "ImagesUser/",
  bucket: "famcamuploads",
  region: "us-east-2",
  accessKey: "AKIAI4LEFCTKJNKI63IQ",
  secretKey: "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",
  successActionStatus: 201
};

var ImagePicker = require('react-native-image-picker');

var options = {
  title: 'Select Avatar',
  quality: 0.2,
  mediaType : 'photo',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

class profileSetup extends Component {
  static navigatorStyle = {
    navBarHidden: true,
  };
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      bio: '',
      visible: false,
      avatarSource : null
    }
  }

  componentWillUnmount() {
    let {actions} = this.props;
    actions.toggleButton(false);
  }

  validationRules= () => {
      return [
          {
              field: this.state.name,
              name: 'Full Name',
              rules: 'required|min:2|max:50'
          },
          {
              field: this.state.bio,
              name: 'Bio',
              rules: 'required|min:1|max:120'
          },
      ]
  }

  back = () => {
    this.props.navigator.pop();
  }


  image = async() => {

    let {avatarSource} = this.state;
    let {actions} = this.props;

    if(Platform.OS == 'android' && Platform.Version > 22){

        const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            ]
        );

        if (granted['android.permission.WRITE_EXTERNAL_STORAGE'] != 'granted' || granted['android.permission.CAMERA'] != 'granted')
            return Alert.alert('', "Don't have permissions to select image.");
    }

  ImagePicker.showImagePicker(options, (response) => {
  console.log('Response = ', response);

  if (response.didCancel) {
    console.log('User cancelled image picker');
  }
  else if (response.error) {
    console.log('ImagePicker Error: ', response.error);
  }
  else if (response.customButton) {
    console.log('User tapped custom button: ', response.customButton);
  }
  else if(!response.error && !response.didCancel) {
     avatarSource = 'data:image/jpeg;base64,' + response.data;
    this.setState({avatarSource});
    console.log(response.path);
    bca=response.uri;
            }
          });
  }

  profile2 = ()=> {
    let {actions} = this.props;
    let {name, bio, visible, avatarSource} = this.state;

    let validaton= Validation.validate(this.validationRules());

    if(validaton.length != 0) {
    return Alert.alert(
    '',
    validaton[0],
    [
      {
        text: 'Okay',
        onPress: ()=> {

        }
      }
    ],
    { cancelable: false }
  );
}
    else {
      this.setState({visible: true})
      actions.getUserName(name);
      var textOrder = "";
            var possible = this.props.user.loginFieldId;
            for (var i = 0; i < 10; i++){
                textOrder += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            const file = {
              // uri can also be a file system path (i.e. file://)
              uri: bca,
              name: textOrder+'.jpg',
              type: "image/jpg"
            }
            RNS3.put(file, option).then(response => {
                if (response.status !== 201)
                  throw new Error("Failed to upload image to S3");
                console.log(response.body);
                actions.getImage(response.body.postResponse.location);
                let values = {'name' : name, 'Bio': bio, 'userId': this.props.user.loginFieldId, 'Profilepicurl' : response.body.postResponse.location}
                console.log(values);
                return axios.post(`${Appurl.apiUrl}setUserProfile`, values)
                .then((response) => {
                  return this.getData(response);
                }).catch((error) => {
                  if(error.response.data.success == 0) {
                    Alert.alert(
                        '',
                        error.response.data.msg,
                        [
                            {
                                    text: 'Okay',
                                    onPress: () => {
                                    this.setState({visible: false});
                            } }
                        ],
                        { cancelable: false }
                    );
                  }
                })
                /**
                 * {
                 *   postResponse: {
                 *     bucket: "your-bucket",
                 *     etag : "9f620878e06d28774406017480a59fd4",
                 *     key: "uploads/image.png",
                 *     location: "https://your-bucket.s3.amazonaws.com/uploads%2Fimage.png"
                 *   }
                 * }
                 */
              });

  }

  }
  getData = async(response) => {
    let {visible} = this.state;
    console.log(response);
    this.setState({visible: false});
    try {
    this.setState({visible: false});
    await AsyncStorage.setItem('user', JSON.stringify(this.props.user.loginFieldId));
    Navigation.startTabBasedApp({
      tabs: [
    {
      label: 'Home',
      screen: 'famcamHome',
      icon: require('./../Images/ic_home.png'),
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
      title: 'Home',
    },
    {
      label: 'Orders',
      screen: 'orders',
      icon: require('./../Images/clipboards.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Orders',
    },
    {
      label: 'Profile',
      screen: 'profile',
      icon: require('./../Images/ic_profile_grey.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Profile',
    },
  ],
  tabsStyle: {
    tabBarButtonColor: '#C54C72',
    tabBarSelectedButtonColor: '#C54C72',
    tabBarBackgroundColor: 'white',
    initialTabIndex: 0,
  },
});
  }
  catch(error) {}
  }

  showImage = () => {
    let {avatarSource} = this.state;
    if(avatarSource) {
      return <Image source = {{uri: avatarSource}} style = {{width:72, height: 72}}/>
    }
    else {
      return <Icon name="camera" color='#9B9B9B' size={25} style={{height: 25, width:25}}/>
    }
  }

  render() {
    let {name, bio, visible, avatarSource} = this.state;
    console.log(this.props.user.userId);
    return (
      <View style={{flex:1, marginHorizontal: 24}}>
        <Spinner visible={visible} cancelable={false} textStyle={{color: '#FFF'}} />
        <View style={{flex: 0.1, justifyContent: 'center'}}>
          <TouchableOpacity style={{height: 20, width:24, justifyContent: 'center'}} onPress={this.back}>
            <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
          </TouchableOpacity>
        </View>
        <View style={{flex:0.08, justifyContent: 'flex-start'}}>
          <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold', color: '#000000'}}>{strings('Profile1.profileText')}</Text>
        </View>
        <View style={{flex:0.1, flexDirection: 'row'}}>
          <TouchableOpacity style={{ height: 72, width: 72, justifyContent: 'center', alignItems: 'center', borderWidth: 0.25, borderColor: '#9B9B9B', borderRadius: 5}} onPress = {() => {this.image()}}>
            {this.showImage()}
          </TouchableOpacity>
          <View style={{flex:0.9, justifyContent: 'center'}}>
            <Text style = {{fontSize: 12, lineHeight: 14, color: '#9B9B9B', fontWeight: '300', marginLeft: 10}}>{strings('Profile1.picLabel')}</Text>
          </View>
        </View>
        <View style={{flex:0.05}}></View>
        <View style={{flex: 0.25}}>
          <View style = {{flex:0.7}}>
            <MKTextField
              placeholder = {strings('Profile1.nameLabel')}
              placeholderTextColor='#AAAFB9'
              floatingLabelEnabled
              keyboardType = "default"
              returnKeyType = "next"
              textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
              style = {{marginTop:10}}
              underlineSize={1}
              highlightColor='#474D57'
              tintColor='#C2567A'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText = {(name) => this.setState({name})}
              onSubmitEditing = {(event) => {this.refs.bioIn.focus();}}
            />
          </View>
          <View style = {{flex:0.8, justifyContent:'flex-start', height: 120, marginTop:15}}>
            <MKTextField
              placeholder = {strings('Profile1.bioLabel')}
              ref="bioIn"
              multiline = {true}
              placeholderTextColor='#AAAFB9'
              floatingLabelEnabled
              keyboardType = "default"
              returnKeyType = "next"
              textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
              style = {{marginTop:10}}
              underlineSize={1}
              highlightColor='#474D57'
              tintColor='#C2567A'
              autoCorrect={false}
              autoCapitalize= 'none'
              onChangeText = {(bio) => this.setState({bio})}
            />
          </View>
        </View>
        <View style = {{flex:0.15,alignItems : 'flex-end', justifyContent: 'center',}}>
          <TouchableOpacity activeOpacity={0.5} onPress={() => {this.profile2()}}>
            <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({

})

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(profileSetup);
