import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import famcam_Home  from './../Components/famcam_home';
import Appurl from './../config';

class loginPassword extends Component {

  constructor(props) {
   super(props);
   this.state = {
     password: '',
     show_password: true,
     visible : false
   }
 }

  static navigatorStyle = {
    navBarHidden : true,
    statusBarHidden : true,
  }

  back = () => {
    this.props.navigator.pop();
  }

  showPassword = () => {
    let {show_password} = this.state;
    if(show_password) {
      this.setState({show_password:false})
    }
    else {
      this.setState({show_password:true})
    }
  }

  checkPassword = () => {
    let {password, visible} = this.state;
    console.log(this.props.user.loginFieldId);
    this.setState({visible: true});
    let values = {'userId' : this.props.user.loginFieldId, 'password' : password};
    return axios.post(`${Appurl.apiUrl}loginUserPassword`, values)
    .then((response) => {
      return this.getLoggedIn(response)
    }).catch((error) => {
      if(error.response.data.success == 0) {
        Alert.alert(
            '',
            error.response.data.msg,
            [
                {
                        text: 'Okay',
                        onPress: () => {
                        this.setState({visible: false});
                } }
            ],
            { cancelable: false }
        );
      }
    })
  }

  getLoggedIn = async(response) => {
    try {
    this.setState({visible: false});
    await AsyncStorage.setItem('user', JSON.stringify(response.data.email));
    Navigation.startTabBasedApp({
      tabs: [
    {
      label: 'Home',
      screen: 'famcamHome',
      icon: require('./../Images/ic_home.png'),
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
      title: 'Home',
    },
    {
      label: 'Orders',
      screen: 'orders',
      icon: require('./../Images/clipboards.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Orders',
    },
    {
      label: 'Profile',
      screen: 'profile',
      icon: require('./../Images/ic_profile_grey.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Profile',
    },
  ],
  tabsStyle: {
    tabBarButtonColor: '#C54C72',
    tabBarSelectedButtonColor: '#C54C72',
    tabBarBackgroundColor: 'white',
    initialTabIndex: 0,
  },
    })
  }
  catch(error) {}
  }

  forgetPassword = () => {
    this.props.navigator.push({
      screen : 'forgotPassword'
    })
  }

  render() {
   let {password, show_password} = this.state;
   return (
     <View style={{flex:1, marginHorizontal: 24}}>

      <Spinner visible={this.state.visible} cancelable={false} textStyle={{color: '#FFF'}} />

       <View style={{flex: 0.1, justifyContent: 'center'}}>
         <TouchableOpacity style={{height: 20, width:24, justifyContent: 'center'}} onPress={() => {this.back()}}>
           <Image source={require('./../Images/icBack.png')} style={{height: 14, width:18}}/>
         </TouchableOpacity>
       </View>
       <View style={{flex:0.08, justifyContent: 'flex-start'}}>
         <Text style = {{fontSize: 24, lineHeight: 32, fontWeight: 'bold'}}>{strings('login.login')}</Text>
       </View>
       <View style={{flex:0.09}}>
         <Text style = {{fontSize: 14, lineHeight: 20, color: '#474D57'}}>{strings('login.heading2', {email: this.props.user.emailPhone})}</Text>
       </View>
       <View style = {{flex:0.15, flexDirection: 'row-reverse'}}>
         <TouchableOpacity style={{justifyContent: 'center', marginBottom: 17, paddingRight:-70}} onPress={() => {this.showPassword()}}>
           <Image source={require('./../Images/ic_eye.png')} style={{height: 12, width: 20}}/>
         </TouchableOpacity>
         <MKTextField
           placeholder = {strings('login.placeholder2')}
           ref="password"
           placeholderTextColor='#474d57'
           floatingLabelEnabled
           password={show_password}
           keyboardType = "default"
           returnKeyType = "next"
           textInputStyle = {{fontSize: 16, lineHeight: 24, color: '#474D57'}}
           style = {{marginTop:10, width: Dimensions.get('window').width*0.8}}
           underlineSize={1}
           highlightColor='#474D57'
           tintColor='red'
           autoCorrect={false}
           autoCapitalize= 'none'
           onChangeText = {(password) => {this.setState({password})}}
         />
       </View>
       <TouchableOpacity style={{flex:0.05, justifyContent: 'flex-start'}} onPress = {() => {this.forgetPassword()}}>
         <Text style={{fontSize: 14, lineHeight: 16, color: 'black', fontWeight: 'bold', marginLeft:10}}> {strings('login.forgotPassword')} </Text>
       </TouchableOpacity>
       <View style = {{flex:0.1,alignItems : 'flex-end'}}>
         <TouchableOpacity activeOpacity={0.5} onPress = {() => {this.checkPassword()}}>
           <Image source = {require('./../Images/fab.png')} style={{height: 56, width: 56}} />
         </TouchableOpacity>
       </View>
     </View>
   )
 }

}


function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(loginPassword);
