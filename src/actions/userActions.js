'use strict';

import * as type from './../constants/ActionTypes';

export function getCountry(val, code) {
  return {
    type : name.COUNTRY_NAME,
    val,
    code
  }
}
//ENDS

export function getPhone(phone) {
  return {
    type : type.GET_PHONE,
    phone
  }
}

export function getUserId(id) {
  console.log(id);
  return {
    type: type.GET_USER_ID,
    id
  }
}

export function getLoginUserId(userid) {
  return {
    type: type.GET_LOGIN_FIELD_ID,
    userid
  }
}

export function getEmail(email) {
  return {
    type: type.GET_USER_EMAIL,
    email
  }
}

export function getCountryCode(countryCode) {
  return {
    type : type.COUNTRY_CODE,
    countryCode
  }
}

export function getLoginField(emailPhone) {
  return {
    type : type.EMAIL_PHONE,
    emailPhone
  }
}

export function getLanguage(lang) {
  console.log(lang);
  return {
    type : type.FETCH_LANG,
    lang
  }
}

export function getTalentId(talentId) {
  return {
    type : type.GET_TALENT_ID,
    talentId,
  }
}

export function getImage(avatarSource) {
  console.log(avatarSource);
  return {
    type: type.GET_PROFILE_PIC,
    avatarSource
  }
}

export function getUserName(name) {
  console.log(name);
  return {
    type: type.GET_USER_NAME,
    name
  }
}

export function getTalentProfession(related) {
  console.log(related);
  return {
    type: type.GET_TALENT_PROFESSION,
    related
  }
}

export function setTalentName(name) {
  console.log(name);
  return {
    type: type.GET_TALENT_NAME,
    name
  }
}

export function setImageTalent(profilepic) {
  return {
    type: type.GET_TALENT_IMAGE,
    profilepic
  }
}

//proile getting details, saving here

export function updateUserName(name) {
  return {
    type: type.FAMCAM_USER_NAME,
    name
  }
}

export function updateUserImage(image) {
  return {
    type: type.FAMCAM_USER_PHOTO,
    image
  }
}

//over


//button disabling after one click
export function toggleButton(bool) {
  return {
    type: type.IS_DISABLED,
    bool
  }
}

export function getPrice(price) {
  console.log(price);
  return {
    type: type.GET_PRICE,
    price
  }
}


export function userToken(token) {
  return {
    type: type.GET_TOKEN,
    token
  }
}
