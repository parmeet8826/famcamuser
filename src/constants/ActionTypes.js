'use strict';

//export const COUNTER_UPDATED = 'COUNTER_UPDATED';
// export const FETCHING_DATA = 'FETCHING_DATA';
// export const FETCHING_DATA_SUCCESS  = 'FETCHING_DATA_SUCCESS';
// export const FETCHING_DATA_FAILURE = 'FETCHING_DATA_FAILURE';
// export const USER = 'USER';
// export const SHOW_ERROR = 'SHOW_ERROR';
// export const SEARCH = 'SEARCH';
// export const COUNTRY_NAME = 'COUNTRY_NAME';
// export const IMAGE = 'IMAGE';
// export const REMINDER = 'REMINDER';
// export const CHANGE_NAME = 'CHANGE_NAME';
// export const CHANGE_BIO = 'CHANGE_BIO';
// export const COVER_PHOTO = 'COVER_PHOTO';
// export const FETCHING_TEMP = 'FETCHING_TEMP';
// export const FETCHING_TEMP_SUCCESS  = 'FETCHING_TEMP_SUCCESS';
// export const FETCHING_TEMP_FAILURE = 'FETCHING_TEMP_FAILURE';

export const FETCH_LANG = 'FETCH_LANG';
export const GET_PHONE = 'GET_PHONE';
export const GET_USER_ID = 'GET_USER_ID';
export const GET_USER_EMAIL = 'GET_USER_EMAIL';
export const COUNTRY_CODE = 'COUNTRY_CODE';
export const GET_PROFILE_PIC = 'PROFILE_PIC';
export const GET_USER_NAME = 'GET_USER_NAME';
export const EMAIL_PHONE = 'EMAIL_PHONE';
export const GET_LOGIN_FIELD_ID = 'GET_LOGIN_FIELD_ID';
export const GET_TALENT_ID = 'GET_TALENT_ID';
export const GET_TALENT_PROFESSION = 'GET_TALENT_PROFESSION';
export const GET_TALENT_NAME = 'GET_TALENT_NAME';
export const GET_TALENT_IMAGE = 'GET_TALENT_IMAGE';


export const FAMCAM_USER_NAME = 'FAMCAM_USER_NAME';
export const FAMCAM_USER_PHOTO = 'FAMCAM_USER_PHOTO';

//button disabling after one hit
export const IS_DISABLED = 'IS_DISABLED';
//over

export const GET_PRICE = 'GET_PRICE';

export const GET_TOKEN = 'GET_TOKEN';
