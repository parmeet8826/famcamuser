'use strict';

import * as type from './../constants/ActionTypes';

const initialState = {
  lang: null,
  phone : '',
  userId : '',
  emailId : '',
  code : '+91',
  emailPhone : '',
  loginFieldId : '',
  talentId : '',
  talentName : '',
  related : '',
  profilepic : '',
  userName : '',
  userBio : '',
  talentImage : '',
  isDisabled : false,
  price : '',


  //profile getting details saving
  famcamUserName : '',
  famcamUserImage : '',
  //over
  token : ''

};

const user = (state = initialState, action) => {
  switch(action.type) {
                case type.FETCH_LANG :
                  return {
                    ...state,
                    lang : action.lang,
                  }
                case type.GET_PHONE :
                  return {
                    ...state,
                    phone : action.phone,
                  }
                case type.GET_USER_ID :
                  return {
                    ...state,
                    userId : action.id,
                  }
                case type.GET_USER_EMAIL :
                  return {
                    ...state,
                    emailId : action.email,
                  }
                case type.COUNTRY_CODE :
                  return {
                    ...state,
                    code : action.countryCode,
                  }
                case type.EMAIL_PHONE :
                  return {
                    ...state,
                    emailPhone : action.emailPhone,
                  }
                case type.GET_LOGIN_FIELD_ID :
                  return {
                    ...state,
                    loginFieldId : action.userid,
                  }
                case type.GET_TALENT_ID :
                  return {
                    ...state,
                    talentId : action.talentId,
                  }
                case type.GET_PROFILE_PIC :
                  return {
                    ...state,
                    profilepic: action.avatarSource
                  }
                case type.GET_USER_NAME :
                  return {
                    ...state,
                    userName : action.name,
                  }
                case type.GET_TALENT_PROFESSION :
                  return {
                    ...state,
                    related: action.related
                  }
                case type.GET_TALENT_NAME :
                  return {
                    ...state,
                    talentName : action.name
                  }
                case type.GET_TALENT_IMAGE :
                  return {
                    ...state,
                    talentImage : action.profilepic
                  }


                case type.FAMCAM_USER_NAME :
                  return {
                    ...state,
                    famcamUserName: action.name,
                  }

                case type.FAMCAM_USER_PHOTO :
                  return {
                    ...state,
                    famcamUserImage: action.image,
                  }
                case type.IS_DISABLED :
                  return {
                    ...state,
                    isDisabled: action.bool
                  }
                case type.GET_PRICE :
                  return {
                    ...state,
                    price: action.price
                  }
                case type.GET_TOKEN :
                  return {
                    ...state,
                    token: action.token
                  }
        default :
          return state;
  }
}

export default user
