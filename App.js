import { Navigation } from 'react-native-navigation';
import { registerScreens } from './screens';
import { AsyncStorage, View } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/configureStore';

const store = configureStore();

registerScreens(store, Provider);

// Navigation.startSingleScreenApp({
//   screen: {
//     screen : 'home'
//   },
// });

AsyncStorage.getItem('user')
.then((response) => {
  if(response == null) {
    Navigation.startSingleScreenApp({
      screen: {
        screen : 'home'
      },
    });
  }
  else {
    Navigation.startTabBasedApp({
      tabs: [
    {
      label: 'Home',
      screen: 'famcamHome',
      icon: require('./Images/ic_home.png'),
      //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
      title: 'Home',
    },
    {
      label: 'Orders',
      screen: 'orders',
      icon: require('./Images/clipboards.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Orders',
    },
    {
      label: 'Profile',
      screen: 'profile',
      icon: require('./Images/ic_profile_grey.png'),
      //selectedIcon: require('../img/two_selected.png'),
      title: 'Profile',
    },
  ],
  tabsStyle: {
    tabBarButtonColor: '#C54C72',
    tabBarSelectedButtonColor: '#C54C72',
    tabBarBackgroundColor: 'white',
    initialTabIndex: 0,
  },
    })
  }
})
