package com.famcam;

import com.facebook.react.ReactPackage;
import com.reactnativenavigation.NavigationApplication;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.auth0.react.A0Auth0Package;
import com.imagepicker.ImagePickerPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.wog.videoplayer.VideoPlayerPackage;
import com.pw.droplet.braintree.BraintreePackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {

     @Override
     public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     protected List<ReactPackage> getPackages() {
         // Add additional packages you require here
         // No need to add RnnPackage and MainReactPackage
         return Arrays.<ReactPackage>asList(
           new RNI18nPackage(),
           new LinearGradientPackage(),
           new A0Auth0Package(),
           new ImagePickerPackage(),
           new ReactNativeOneSignalPackage(),
           new VideoPlayerPackage(),
           new BraintreePackage(),
           new SplashScreenReactPackage()
         );
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
         return getPackages();
     }
 }
