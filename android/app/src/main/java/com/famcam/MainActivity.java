package com.famcam;
import android.os.Bundle; // here
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
import com.reactnativenavigation.controllers.SplashActivity;
import android.content.Intent;

 public class MainActivity extends SplashActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
       SplashScreen.show(this);  // here
       super.onCreate(savedInstanceState);
   }

 }
