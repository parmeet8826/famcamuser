import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  ScrollView,
  RefreshControl,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';

var id;

class requests extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pendingArr : [],
      rejectArr : [],
      completeArr : [],
      refreshing : false,
    }
    let {actions} = this.props;
    AsyncStorage.getItem('user')
    .then((user) => {
        let details = JSON.parse(user);
        id = user.id;
        actions.getLoginUserId(details);
        console.log(details);
    });
    this.requestData();
  }

  componentWillMount() {
    console.log(this.props.user.loginFieldId.id);
  }

  requestData = () => {

    let values = {'userId' : id}
    console.log(values);
    return axios.post(`${Appurl.apiUrl}userRequestStatusData`, values)
    .then((response) => {
          console.log(response);
          return this.showRequests(response.data.data);
    }).catch((error) => {
      console.log(error.response);
    });
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.getData();
  }

  getData = () => {
    let values = {'userId' : id}
    console.log(values);
    return axios.post(`${Appurl.apiUrl}userRequestStatusData`, values)
    .then((response) => {
          console.log(response);
          return this.showRequests(response.data.data);
    }).catch((error) => {
      console.log(error.response);
    });
  }

  showRequests = (response) => {
    console.log(response);
    this.setState({refreshing: false});
    let {pendingArr, rejectArr} = this.state;
    pendingArr.splice(0, pendingArr.length);
    rejectArr.splice(0, rejectArr.length);
    this.setState({pendingArr, rejectArr});

    for(let i = 0; i < response.length; i++) {
    if(response[i].isPending === true) {
      pendingArr.push({'forWhome': response[i].forWhome, 'msg' : response[i].message, 'talentName': response[i].talentName, 'time': response[i].time, 'image': response[i].talentImage})
      this.setState({pendingArr});
  }
  else {
    rejectArr.push({'forWhome': response[i].forWhome, 'msg' : response[i].message, 'talentName': response[i].talentName, 'time': response[i].time, 'image': response[i].talentImage})
    this.setState({rejectArr});
  }
}
  }

  render() {
    let {pendingArr, dummy, rejectArr, completeArr} = this.state;
    console.log(pendingArr);
    return (
      <ScrollView style = {{flex:1}}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
            colors={['#BF4D73', '#D8546E', '#8D3F7D']}
            title = "Loading"
          />
        }
      >
        <View style = {{flex:0.3, height: 50, justifyContent:'flex-start', alignItems:'center', flexDirection:'row', marginTop: 20, borderBottomWidth:0.3, marginHorizontal: 24}}>
          <Text style = {{fontSize: 16, lineHeight:24, fontWeight:'bold', color: '#4A4A4A'}}>{strings('status.pending')}</Text>
        </View>

          {
            pendingArr ? pendingArr.map((value,index) => {
              return <View key = {index} style = {{flex:0.7, marginHorizontal: 24, marginTop:20}}>
                <View style = {{flex: 0.3, flexDirection:'row', justifyContent:'space-between'}}>
                  <View>
                    <Text style = {{fontWeight:'bold', fontSize: 20, lineHeight: 24, color: '#1F1D1D'}}>{value.talentName}</Text>
                    <Text style = {{fontSize: 16, lineHeight: 24, color : '#F5A623'}}>{strings('status.waiting')}</Text>
                  </View>
                    <Image source = {{uri: value.image}} style = {{width: 48, height: 48, borderRadius: 24}}/>
                </View>

                <View style = {{flex:0.1}}>
                  <Text style = {{fontWeight:'bold', fontSize: 12, lineHeight: 16, color : '#474D57'}}>{strings('status.for')}</Text>
                  <Text style = {{fontSize: 14, lineHeight: 24, color : '#000000'}}>{value.forWhome}</Text>
                </View>

                <View style = {{flex:0.3, marginTop:12}}>
                  <Text style = {{fontSize: 12,fontWeight:'bold', lineHeight: 16, color: '#474D57'}}>{strings('status.message')}</Text>
                  <Text style = {{fontSize:14, lineHeight: 20}}>{value.msg}</Text>
                </View>

                <View style = {{flex:0.2, marginTop:10}}>
                  <Text style = {{fontWeight:'bold', fontSize: 12, lineHeight: 16, color:'#7F7F7F'}}>{strings('status.order')}</Text>
                  <Text style = {{fontSize: 14, lineHeight: 24}}>{value.order}</Text>
                </View>

                <View style = {{flex: 0.1, marginTop: 12}}>
                  <Text style = {{fontSize:12, lineHeight:16}}>{value.time}</Text>
                </View>

              </View>
            }) : null
          }

        <View style = {{flex:0.3, height: 50, justifyContent:'flex-start', alignItems:'center', flexDirection:'row', marginTop: 20, borderBottomWidth:0.3, marginHorizontal: 24}}>
          <Text style = {{fontSize: 16, lineHeight:24, fontWeight:'bold', color: '#4A4A4A'}}>{strings('status.rejected')}</Text>
        </View>

        { rejectArr ? rejectArr.map((value,index) => {
          return <View key = {index} style = {{flex:0.7, marginHorizontal: 24, marginTop:20}}>
            <View style = {{flex: 0.3, flexDirection:'row', justifyContent:'space-between'}}>
              <View>
                <Text style = {{fontWeight:'bold', fontSize: 20, lineHeight: 24, color: '#1F1D1D'}}>{value.talentName}</Text>
                <Text style = {{fontSize: 16, lineHeight: 24, color : '#F5A623'}}>Waiting For Reply</Text>
              </View>
                <Image source = {{uri: value.image}} style = {{width: 48, height: 48, borderRadius: 24}}/>
            </View>

            <View style = {{flex:0.1}}>
              <Text style = {{fontWeight:'bold', fontSize: 12, lineHeight: 16, color : '#474D57'}}>For</Text>
              <Text style = {{fontSize: 14, lineHeight: 24, color : '#000000'}}>{value.forWhome}</Text>
            </View>

            <View style = {{flex:0.3, marginTop:12}}>
              <Text style = {{fontSize: 12,fontWeight:'bold', lineHeight: 16, color: '#474D57'}}>MESSAGE</Text>
              <Text style = {{fontSize:14, lineHeight: 20}}>{value.msg}</Text>
            </View>

            <View style = {{flex:0.2, marginTop:10}}>
              <Text style = {{fontWeight:'bold', fontSize: 12, lineHeight: 16, color:'#7F7F7F'}}>ORDER NUMBER</Text>
              <Text style = {{fontSize: 14, lineHeight: 24}}>{value.order}</Text>
            </View>

            <View style = {{flex: 0.1, marginTop: 12}}>
              <Text style = {{fontSize:12, lineHeight:16}}>{value.time}</Text>
            </View>

          </View>
        }) : null
      }

        </ScrollView>
    )
  }
}


function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(requests);
