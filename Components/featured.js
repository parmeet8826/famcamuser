import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  ScrollView,
  AsyncStorage
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import axios from 'axios';
import Appurl from './../config';

class featured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      result : []
    }
  }

  componentWillMount() {
    this.showTalent();
  }

  showTalent = () => {
    return fetch(`${Appurl.apiUrl}displayAllTalentToUserAccount`)
    .then((response) => response.json())
    .then((responseJson) => {
      return this.displayData(responseJson.msg);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  displayData = (response) => {
    console.log(response);
    let {result} = this.state;
    response.forEach((item, index) => {
      result.push({id : item._id, name : item.name, profession: item.professions, pic : item.profilePicUrl});
    })
    this.setState({result});
  }

  talents = (talentId, related, name) => {
    let {actions} = this.props;
    actions.getTalentId(talentId);
    actions.getTalentProfession(related);
    this.props.navigator.push({
      screen : 'talentInfo'
    })
  }

  render() {
    let {result} = this.state;
    return (
      <View style = {{flex:0.8, marginHorizontal: 14, marginTop:20}}>
        <Text style = {{color : '#4A4A4A', fontSize: 30, fontWeight:'bold'}}>Featured</Text>
        <ScrollView showsVerticalScrollIndicator = {false}>
          {
            result.map((value, index) => {
              return <View key = {index} style = {{flex:0.2, marginTop: 20}}>
                  <TouchableOpacity activeOpacity={0.9} onPress = {() => {this.talents(value.id, value.profession[0], value.name)}}>
                  <ImageBackground source = {{uri : value.pic}} style = {{width:400, height: 200}}>
                    <View style = {{flex:0.9,justifyContent:'flex-end', marginHorizontal: 10}}>
                      <Text style = {{color: 'white', fontSize: 20, fontWeight:'bold'}}>{value.name}</Text>
                      <Text style = {{color: 'white'}}>{value.profession[0]} {value.profession[1]}</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            })
          }
        </ScrollView>
      </View>
    )
  }

}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(featured);
