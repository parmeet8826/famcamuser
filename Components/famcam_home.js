import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  TextInput
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import { MKTextField } from 'react-native-material-kit';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class famcam_home extends Component {


  switchToTabBased = () => {
  //   Navigation.startTabBasedApp({
  //     tabs: [
  //   {
  //     label: 'Home',
  //     screen: 'famcamHome',
  //     icon: require('./../Images/ic_home.png'),
  //     //selectedIcon: require('../img/one_selected.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
  //     title: 'Home',
  //   },
  //   {
  //     label: 'Orders',
  //     screen: 'orders',
  //     icon: require('./../Images/clipboards.png'),
  //     //selectedIcon: require('../img/two_selected.png'),
  //     title: 'Orders',
  //   },
  //   {
  //     label: 'Profile',
  //     screen: 'profile',
  //     icon: require('./../Images/ic_profile_grey.png'),
  //     //selectedIcon: require('../img/two_selected.png'),
  //     title: 'Profile',
  //   },
  // ],
  // tabsStyle: {
  //   tabBarButtonColor: '#C54C72',
  //   tabBarSelectedButtonColor: '#C54C72',
  //   tabBarBackgroundColor: 'white',
  //   initialTabIndex: 0,
  // },
  //   })

  return <View><Text>Hello</Text></View>

  }


  render() {
    return (
      <View style = {{flex:1}}>
        {this.switchToTabBased()}
      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(famcam_home);
