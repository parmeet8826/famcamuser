import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert,
  ScrollView,
  RefreshControl,
  AsyncStorage
} from 'react-native';
import { strings } from '../locales/i18n';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import * as userActions from '../src/actions/userActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Appurl from './../config';

var VideoPlayer = require('react-native-native-video-player');

class received extends Component {

  constructor(props) {
    super(props);
    this.state = {
      results : [],
      refreshing : false,
      videos : [
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/+o0biha0fhht+n+.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/0ef4n++s5ba4uca.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg',
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/2fJJ7Jabs50s33a.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a2g6af8fe7va217.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      },
      {
        link : 'https://s3.us-east-2.amazonaws.com/famcamuploads/videos/a9a58a8428dc1a2.mp4',
        image : 'https://s3.us-east-2.amazonaws.com/famcamuploads/ImagesTalent/f6e23e401211fdd.jpg'
      }
    ]
    }
    let {actions} = this.props;
    AsyncStorage.getItem('user')
    .then((user) => {
        let details = JSON.parse(user);
        actions.getLoginUserId(details);
        console.log(details);
    });
    this.receivedData();
  }

  //'5a82cdc7b72f2a0004da41ef'
  componentWillMount() {
    console.log(this.props.user.loginFieldId.id);
  }
  receivedData = () => {
    
    let values = {'userId' : this.props.user.loginFieldId.id}
    return axios.post(`${Appurl.apiUrl}UserVediosUrl`, values)
    .then((response) => {
      return this.getResponse(response.data.data);
    }).catch((error) => {
      console.log(error.response);
    })
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this.getData();
  }

  getData = () => {
    let values = {'userId' : this.props.user.loginFieldId.id}
    return axios.post(`${Appurl.apiUrl}UserVediosUrl`, values)
    .then((response) => {
      return this.getResponse(response.data.data);
    }).catch((error) => {
      console.log(error.response);
    })
  }

  getResponse(data) {
    this.setState({refreshing: false});
    console.log(data);
    let {results} = this.state;
    results.splice(0,results.length);
    this.setState({results});


    for(let i = 0; i < data.length; i++) {
      results.push({'for': data[i].forWhome, 'message': data[i].message, 'talentName': data[i].talentName, 'duration': data[i].duration, 'video': data[i].vedioUrl, 'time': data[i].time, 'image': data[i].thumbnailUrl});
    }
    this.setState({results});
  }

  playVideo = (url) => {
    VideoPlayer.showVideoPlayer(url);
  }

  render() {
    let {results, videos} = this.state;
    return (
      <View style = {{flex:1, marginHorizontal: 24}}>
         <ScrollView style = {{flex:1}} showsVerticalScrollIndicator = {false}
           refreshControl={
             <RefreshControl
               refreshing={this.state.refreshing}
               onRefresh={this._onRefresh.bind(this)}
               colors={['#BF4D73', '#D8546E', '#8D3F7D']}
               title = "Loading"
             />
           }
         >

           {
             results.map((value,index) => {
               return <View style = {{flex:1}} key = {index}>

                 <View style = {{width: 312, height: 232, marginTop:24, flexDirection:'row', borderBottomWidth:0.3, borderBottomColor : '#ACACAC'}}>
                   <View style = {{width:126, height: 208}}>
                       <TouchableOpacity activeOpacity = {0.8} onPress = {() => {this.playVideo(value.video)}}>
                       <Image source = {{uri : value.image}} style = {{width:126, height:208, borderRadius: 4}}/>
                       <Image source  = {require('./../Images/GroupCopy3x.png')} style = {{width:24, height: 24, position: 'absolute', top: 176, left:5}}/>
                       <Text style = {{fontSize:12, lineHeight:16, fontWeight:'bold', color : 'white', position:'absolute', top:10, left:90}}>{value.duration}</Text>
                     </TouchableOpacity>
                   </View>
                   <View style = {{marginLeft: 10, width: 130}}>
                     <Text style = {{fontSize: 20, lineHeight: 24, color : '#1F1D1D', fontWeight:'bold', height:40}}>{value.talentName}</Text>
                     <Text style = {{fontWeight:'bold', fontSize:14, lineHeight: 16, color : '#474D57', opacity: 0.5}}>{strings('status.for')}</Text>
                     <Text style = {{fontSize:14, lineHeight: 24, color: 'black', height: 50}}>{value.for}</Text>
                     <Text style = {{fontSize:14, lineHeight: 16, color : '#474D57', fontWeight:'bold', opacity: 0.5, height:20}}>{strings('status.message')}</Text>
                     <Text style = {{fontSize:14, lineHeight: 20, color : 'black', opacity: 0.8}}>{value.message}</Text>
                     <Text style = {{fontSize:12, lineHeight: 16, color : '#7F7F7F', marginTop: 20}}>{value.time}</Text>
                   </View>
                 </View>

               </View>
             })
           }

         </ScrollView>
      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(received);
